package cc.nctu1210.api.koala6x;

import android.util.Log;

import java.util.Arrays;

/**
 * Created by fariz on 31-Jul-16.
 */
public class KoalaGesture {
    private double[][] koalaData = new double[6][180];

    public void setKoalaData(double[][] input){
       koalaData = input;
    }

    public String getGesture(){
        String gestureDecision = "";
        double[] Class = new double[11];
        final int indexAx = 0;
        final int indexAy = 1;
        final int indexAz = 2;
        final int indexGx = 3;
        final int indexGy = 4;
        final int indexGz = 5;
        final int indexTotalAcc = 6;
        final int indexTotalGyro = 7;
        final int indexRoll = 8;
        final int indexPitch = 9;
        final double koalaSamplingRate = 80;

        double[] totalAcc = getTotalAxes(koalaData[indexAx],koalaData[indexAy],koalaData[indexAz]);
        double[] totalGyro = getTotalAxes(koalaData[indexGx],koalaData[indexGy],koalaData[indexGz]);
        double[] roll = getRoll(koalaData[indexAx],koalaData[indexAz]);
        double[] pitch = getPitch(koalaData[indexAy],koalaData[indexAz]);

        double[][] processedKoalaData = new double[10][180];
        for(int a=0; a<6;a++){
            processedKoalaData[a] = Arrays.copyOf(koalaData[a],koalaData[a].length);
        }

        processedKoalaData[6] = Arrays.copyOf(totalAcc,totalAcc.length);
        processedKoalaData[7] = Arrays.copyOf(totalGyro,totalGyro.length);
        processedKoalaData[8] = Arrays.copyOf(roll,roll.length);
        processedKoalaData[9] = Arrays.copyOf(pitch,pitch.length);

        double[] standardDeviation = getStandardDeviation2D(processedKoalaData);
        double[] mean = getMean2D(processedKoalaData);
        double[] variance = getVariance2D(processedKoalaData);
        double[] zeroCrossingRate = getZeroCrossingRate2D(processedKoalaData);
        double[] rootMeanSquare = getRootMeanSquare2D(processedKoalaData);
        double[] skewness = getSkewness2D(processedKoalaData);
        double[] kurtosis = getKurtosis2D(processedKoalaData);
        double[] meanAbsDev = getMeanAbsDev2D(processedKoalaData);

        double[] fftMaxFreq = getFFTMaxFreq2D(processedKoalaData,koalaSamplingRate);
        double[] fftEnergy = getFFTEnergy2D(processedKoalaData);
        double[] fftEntropy = getFFTEntropy2D(processedKoalaData);

        double covarianceAxAy = getCovariance(processedKoalaData[indexAx],processedKoalaData[indexAz]);
        double covarianceAxAz = getCovariance(processedKoalaData[indexAx],processedKoalaData[indexAz]);
        double covarianceAyAz = getCovariance(processedKoalaData[indexAy],processedKoalaData[indexAz]);
        double covarianceGxGy = getCovariance(processedKoalaData[indexGx],processedKoalaData[indexGy]);
        double covarianceGxGz = getCovariance(processedKoalaData[indexGx],processedKoalaData[indexGz]);
        double covarianceGyGz = getCovariance(processedKoalaData[indexGy],processedKoalaData[indexGz]);

        double correlationAxAy = getCorrelation(processedKoalaData[indexAx],processedKoalaData[indexAz]);
        double correlationAxAz = getCorrelation(processedKoalaData[indexAx],processedKoalaData[indexAz]);
        double correlationAyAz = getCorrelation(processedKoalaData[indexAy],processedKoalaData[indexAz]);
        double correlationGxGy = getCorrelation(processedKoalaData[indexGx],processedKoalaData[indexGy]);
        double correlationGxGz = getCorrelation(processedKoalaData[indexGx],processedKoalaData[indexGz]);
        double correlationGyGz = getCorrelation(processedKoalaData[indexGy],processedKoalaData[indexGz]);
        //MODEL
        Class[0] = -33.161816 + standardDeviation[indexAx] * -4.977513 + standardDeviation[indexGz] * -0.003907 + standardDeviation[indexRoll] * 0.01817 + mean[indexAx] * 7.656552 + mean[indexAy] * 2.340809 + mean[indexAz] * 1.064188 + mean[indexGx] * 0.016861 + mean[indexGy] * 0.06882 + mean[indexGz] * -0.035896 + mean[indexTotalAcc] * 6.436411 + mean[indexPitch] * -0.01774 + variance[indexAz] * 5.09834 + variance[indexGx] * -0.000079 + variance[indexGz] * -0.0004 + variance[indexRoll] * 0.00052 + zeroCrossingRate[indexAx] * 8.251577 + zeroCrossingRate[indexAz] * 56.073715 + zeroCrossingRate[indexGx] * 8.981383 + zeroCrossingRate[indexGz] * 2.948298 + rootMeanSquare[indexAy] * -7.421294 + rootMeanSquare[indexAz] * 22.036771 + rootMeanSquare[indexGz] * -0.006593 + skewness[indexAx] * 0.276623 + skewness[indexAy] * -0.180019 + skewness[indexAz] * 0.157073 + skewness[indexGx] * -0.029163 + skewness[indexGy] * 0.037543 + skewness[indexGz] * -0.218545 + skewness[indexTotalGyro] * 0.613604 + skewness[indexRoll] * -0.17165 + skewness[indexPitch] * -0.163541 + kurtosis[indexAx] * -0.020042 + kurtosis[indexAy] * 0.065131 + kurtosis[indexAz] * 0.038562 + kurtosis[indexGx] * -1.325225 + kurtosis[indexGy] * 0.079325 + kurtosis[indexGz] * 0.165544 + kurtosis[indexTotalAcc] * -0.244475 + kurtosis[indexPitch] * 0.009502 + meanAbsDev[indexTotalAcc] * 1.053214 + fftMaxFreq[indexAx] * 0.19866 + fftMaxFreq[indexAy] * -0.928615 + fftMaxFreq[indexAz] * 2.956977 + fftMaxFreq[indexGx] * -0.632529 + fftMaxFreq[indexGz] * -0.634746 + fftMaxFreq[indexRoll] * 0.579217 + fftMaxFreq[indexPitch] * -0.642083 + fftEnergy[indexAx] * 7.911549 + fftEnergy[indexAy] * -5.896965 + fftEnergy[indexAz] * 3.009284 + fftEnergy[indexGx] * -0.00005 + fftEnergy[indexGy] * -0.0002 + fftEnergy[indexTotalGyro] * -0.000006 + fftEnergy[indexPitch] * -0.000075 + fftEntropy[indexAx] * 0.011033 + fftEntropy[indexAy] * 0.017708 + fftEntropy[indexAz] * 0.041483 + fftEntropy[indexGz] * 0.000052 + fftEntropy[indexTotalAcc] * -0.022822 + fftEntropy[indexRoll] * -0.000026 + fftEntropy[indexPitch] * 0.000018 + covarianceAxAy * 0.827157 + covarianceAyAz * 24.738845 + covarianceGxGz * -0.000672 + covarianceGyGz * 0.000415 + correlationAxAy * 1.606965 + correlationGxGz * 0.550716 + correlationGyGz * -0.92045 ;
        Class[1] = 0.074498 + standardDeviation[indexAx] * 12.066619 + standardDeviation[indexGx] * 0.050206 + standardDeviation[indexGy] * 0.009095 + standardDeviation[indexTotalAcc] * 32.290859 + standardDeviation[indexRoll] * -0.266712 + mean[indexAz] * -6.763769 + mean[indexGx] * -0.048541 + mean[indexGy] * 0.025829 + mean[indexGz] * -0.039886 + mean[indexTotalAcc] * -2.398256 + mean[indexPitch] * 0.047695 + variance[indexAy] * 5.02588 + variance[indexAz] * -41.346417 + variance[indexGy] * -0.000153 + variance[indexGz] * -0.000975 + variance[indexTotalGyro] * -0.000947 + variance[indexRoll] * -0.005935 + zeroCrossingRate[indexAx] * -2.045844 + zeroCrossingRate[indexAy] * -8.799799 + zeroCrossingRate[indexGx] * -24.032852 + zeroCrossingRate[indexGy] * -21.827045 + zeroCrossingRate[indexGz] * 5.410486 + zeroCrossingRate[indexPitch] * -3.553967 + rootMeanSquare[indexAx] * 1.226425 + rootMeanSquare[indexGx] * 0.015488 + rootMeanSquare[indexGz] * -0.034453 + rootMeanSquare[indexTotalAcc] * -1.083253 + skewness[indexAx] * -0.666893 + skewness[indexAy] * 2.136908 + skewness[indexAz] * -0.723297 + skewness[indexGy] * 0.218452 + skewness[indexTotalAcc] * -2.255723 + kurtosis[indexAz] * -1.005426 + kurtosis[indexGy] * 0.168677 + kurtosis[indexGz] * 0.167664 + kurtosis[indexPitch] * -0.7658 + meanAbsDev[indexAx] * -10.929301 + meanAbsDev[indexAy] * 5.711437 + meanAbsDev[indexPitch] * 0.131496 + fftMaxFreq[indexAx] * -3.046302 + fftMaxFreq[indexAy] * 1.30638 + fftMaxFreq[indexGx] * 0.879766 + fftMaxFreq[indexGy] * -0.847475 + fftMaxFreq[indexGz] * 1.783474 + fftMaxFreq[indexRoll] * -0.641028 + fftEnergy[indexAy] * -3.835806 + fftEnergy[indexAz] * 3.062634 + fftEnergy[indexGz] * -0.000158 + fftEnergy[indexRoll] * -0.001273 + fftEntropy[indexAy] * -0.006541 + fftEntropy[indexAz] * -0.043142 + fftEntropy[indexGx] * 0.00002 + fftEntropy[indexGy] * -0.000243 + fftEntropy[indexGz] * 0.00028 + covarianceGxGy * 0.000496 + covarianceGxGz * -0.00141 + covarianceGyGz * -0.000348 + correlationAxAy * 1.151629 + correlationGxGz * -1.34024 + correlationGyGz * -1.092323 ;
        Class[2] = -38.285626 + standardDeviation[indexAy] * 1.837654 + standardDeviation[indexAz] * 5.679503 + standardDeviation[indexGx] * 0.018953 + standardDeviation[indexGy] * 0.023695 + standardDeviation[indexGz] * 0.046623 + standardDeviation[indexRoll] * 0.054485 + mean[indexAx] * 10.107398 + mean[indexAz] * 8.273781 + mean[indexGy] * -0.070219 + mean[indexGz] * -0.002851 + mean[indexTotalAcc] * 23.661148 + variance[indexAx] * -20.109323 + variance[indexAz] * -122.782173 + variance[indexGx] * -0.000874 + variance[indexGz] * 0.000233 + variance[indexTotalAcc] * -22.807135 + variance[indexTotalGyro] * -0.000105 + zeroCrossingRate[indexAx] * 8.142993 + zeroCrossingRate[indexGx] * 21.590274 + zeroCrossingRate[indexGz] * 2.095327 + zeroCrossingRate[indexPitch] * -1.294737 + rootMeanSquare[indexAx] * 3.583003 + rootMeanSquare[indexAy] * 4.586154 + rootMeanSquare[indexGx] * -0.010827 + skewness[indexAx] * -1.520357 + skewness[indexAy] * 0.690628 + skewness[indexGx] * 0.08553 + skewness[indexGy] * 0.404175 + skewness[indexGz] * -0.493858 + skewness[indexTotalAcc] * -0.282286 + kurtosis[indexAz] * 0.044019 + kurtosis[indexGx] * 0.022266 + kurtosis[indexGy] * 0.021907 + kurtosis[indexGz] * -0.261916 + kurtosis[indexTotalAcc] * 0.421505 + kurtosis[indexTotalGyro] * -0.155485 + kurtosis[indexRoll] * -0.789557 + kurtosis[indexPitch] * 0.094602 + meanAbsDev[indexAx] * 3.734921 + meanAbsDev[indexAz] * -13.444379 + meanAbsDev[indexGx] * 0.032784 + meanAbsDev[indexTotalGyro] * 0.014889 + meanAbsDev[indexRoll] * 0.024113 + fftMaxFreq[indexAx] * -0.891123 + fftMaxFreq[indexAy] * 1.291751 + fftMaxFreq[indexGx] * 0.144004 + fftMaxFreq[indexGz] * -0.488565 + fftMaxFreq[indexRoll] * 0.210671 + fftMaxFreq[indexPitch] * 0.731125 + fftEnergy[indexAx] * -1.706297 + fftEnergy[indexGz] * -0.000112 + fftEnergy[indexTotalGyro] * -0.000015 + fftEnergy[indexPitch] * -0.000126 + fftEntropy[indexAx] * -0.038996 + fftEntropy[indexAy] * -0.01171 + fftEntropy[indexAz] * -0.046231 + fftEntropy[indexGx] * -0.000118 + fftEntropy[indexGy] * 0.000016 + fftEntropy[indexGz] * -0.000029 + fftEntropy[indexTotalAcc] * 0.035461 + fftEntropy[indexPitch] * 0.000426 + covarianceAxAy * 6.090668 + covarianceAyAz * 47.944901 + covarianceGxGy * 0.00197 + covarianceGyGz * 0.000276 + correlationAyAz * -1.071984 + correlationGxGy * -2.167864 + correlationGxGz * -0.426407 ;
        Class[3] = 1.85906 + standardDeviation[indexAy] * -3.641157 + standardDeviation[indexGy] * 0.025147 + standardDeviation[indexTotalAcc] * -6.800547 + standardDeviation[indexTotalGyro] * 0.02787 + standardDeviation[indexRoll] * 0.18007 + mean[indexAx] * -11.240366 + mean[indexGx] * 0.091393 + mean[indexTotalAcc] * 18.019277 + variance[indexAx] * 11.117199 + variance[indexAy] * -20.676983 + variance[indexGz] * 0.000137 + variance[indexTotalAcc] * -25.462464 + zeroCrossingRate[indexAx] * -1.794655 + zeroCrossingRate[indexAy] * 4.515024 + zeroCrossingRate[indexGx] * 22.292087 + zeroCrossingRate[indexGy] * 14.344275 + zeroCrossingRate[indexGz] * -16.305158 + zeroCrossingRate[indexRoll] * -11.512818 + rootMeanSquare[indexAx] * 6.805844 + rootMeanSquare[indexGx] * 0.060196 + rootMeanSquare[indexRoll] * 0.035271 + skewness[indexAx] * 1.401556 + skewness[indexAy] * 0.308268 + skewness[indexAz] * 0.117692 + skewness[indexGx] * -1.074932 + skewness[indexGy] * 0.114342 + skewness[indexRoll] * -1.011234 + skewness[indexPitch] * -0.202045 + kurtosis[indexAx] * -0.332007 + kurtosis[indexAy] * -0.302196 + kurtosis[indexAz] * -0.549187 + kurtosis[indexGx] * 0.155856 + kurtosis[indexGz] * -1.641683 + kurtosis[indexTotalAcc] * -0.880683 + kurtosis[indexTotalGyro] * -0.384178 + kurtosis[indexRoll] * -1.295593 + kurtosis[indexPitch] * 0.13992 + meanAbsDev[indexAz] * 5.754701 + meanAbsDev[indexGy] * -0.103007 + meanAbsDev[indexGz] * 0.07906 + meanAbsDev[indexTotalAcc] * -7.5983 + meanAbsDev[indexTotalGyro] * 0.036683 + meanAbsDev[indexRoll] * -0.396317 + fftMaxFreq[indexAy] * -0.726788 + fftMaxFreq[indexGx] * -0.601169 + fftMaxFreq[indexGy] * 0.201826 + fftMaxFreq[indexGz] * -1.002669 + fftMaxFreq[indexRoll] * 0.960159 + fftEnergy[indexAy] * -7.077507 + fftEnergy[indexTotalAcc] * -9.641493 + fftEntropy[indexAx] * -0.025554 + fftEntropy[indexAy] * -0.084296 + fftEntropy[indexAz] * 0.129206 + fftEntropy[indexGy] * 0.000088 + fftEntropy[indexTotalAcc] * 0.038483 + fftEntropy[indexTotalGyro] * -0.00002 + fftEntropy[indexPitch] * 0.000628 + covarianceAxAy * -9.76895 + covarianceAyAz * 28.003084 + covarianceGxGy * 0.003862 + covarianceGxGz * 0.000398 + correlationAxAy * -2.168593 + correlationAyAz * -6.978476 + correlationGxGy * -5.067878 + correlationGxGz * 0.807836 ;
        Class[4] = -8.517098 + standardDeviation[indexAz] * 1.351603 + standardDeviation[indexGx] * 0.080684 + standardDeviation[indexGy] * 0.059673 + standardDeviation[indexGz] * 0.011445 + standardDeviation[indexTotalAcc] * 18.721447 + standardDeviation[indexPitch] * 0.009911 + mean[indexAy] * -0.544139 + mean[indexAz] * 2.740393 + mean[indexGx] * -0.03164 + mean[indexGy] * -0.10673 + mean[indexGz] * 0.003732 + mean[indexTotalAcc] * -0.694765 + mean[indexPitch] * 0.058876 + variance[indexAy] * -2.93487 + variance[indexGy] * 0.000264 + variance[indexGz] * -0.000206 + variance[indexTotalGyro] * -0.001013 + zeroCrossingRate[indexAz] * -27.649642 + zeroCrossingRate[indexGx] * 7.29586 + zeroCrossingRate[indexGz] * -35.532512 + zeroCrossingRate[indexRoll] * -6.04044 + zeroCrossingRate[indexPitch] * -10.184604 + rootMeanSquare[indexAx] * -1.072273 + rootMeanSquare[indexAz] * 4.756526 + rootMeanSquare[indexGx] * 0.046714 + rootMeanSquare[indexTotalAcc] * -8.386036 + rootMeanSquare[indexRoll] * -0.033319 + skewness[indexAx] * -0.403289 + skewness[indexAy] * 2.666303 + skewness[indexAz] * -2.047703 + skewness[indexGx] * -0.80005 + skewness[indexGy] * -0.654297 + skewness[indexGz] * 0.109484 + skewness[indexTotalAcc] * 0.183324 + skewness[indexPitch] * 0.910538 + kurtosis[indexAx] * -0.677391 + kurtosis[indexAy] * -0.092154 + kurtosis[indexAz] * -0.381239 + kurtosis[indexGz] * -0.339136 + kurtosis[indexTotalAcc] * -0.165076 + kurtosis[indexTotalGyro] * 0.212201 + kurtosis[indexRoll] * -0.026855 + kurtosis[indexPitch] * -0.624502 + meanAbsDev[indexAy] * 2.58015 + meanAbsDev[indexAz] * 3.371601 + meanAbsDev[indexRoll] * 0.02166 + fftMaxFreq[indexAx] * 0.392143 + fftMaxFreq[indexAy] * 1.27095 + fftMaxFreq[indexGx] * 1.794566 + fftMaxFreq[indexGy] * -0.894998 + fftMaxFreq[indexGz] * -0.924021 + fftEnergy[indexAy] * -1.723625 + fftEnergy[indexAz] * -0.461052 + fftEnergy[indexGx] * -0.000104 + fftEnergy[indexGy] * 0.000055 + fftEnergy[indexGz] * -0.000503 + fftEnergy[indexTotalGyro] * -0.000014 + fftEntropy[indexAz] * 0.026532 + fftEntropy[indexGx] * 0.00003 + fftEntropy[indexGy] * -0.000164 + fftEntropy[indexRoll] * 0.000051 + covarianceAxAy * 9.957813 + covarianceGxGy * 0.000195 + covarianceGyGz * -0.000529 + correlationAxAy * 1.896608 + correlationAyAz * 0.257823 + correlationGxGz * -0.641475 + correlationGyGz * -0.302178 ;
        Class[5] = -3.038207 + standardDeviation[indexAx] * 10.864187 + standardDeviation[indexAy] * 13.774959 + standardDeviation[indexGx] * 0.00808 + standardDeviation[indexGz] * 0.014567 + standardDeviation[indexTotalAcc] * 3.786941 + standardDeviation[indexTotalGyro] * 0.005197 + standardDeviation[indexPitch] * 0.010111 + mean[indexAx] * -0.863832 + mean[indexAy] * -9.578653 + mean[indexAz] * 0.69506 + mean[indexGx] * 0.04973 + mean[indexGy] * 0.190932 + mean[indexGz] * 0.004342 + mean[indexTotalAcc] * -3.087057 + variance[indexGx] * -0.000017 + variance[indexGy] * -0.000441 + variance[indexTotalGyro] * -0.001307 + variance[indexRoll] * -0.003212 + variance[indexPitch] * -0.000728 + zeroCrossingRate[indexAx] * -4.289824 + zeroCrossingRate[indexAy] * 16.945933 + zeroCrossingRate[indexAz] * -125.564658 + zeroCrossingRate[indexGx] * -29.187514 + zeroCrossingRate[indexGy] * -18.250536 + zeroCrossingRate[indexGz] * 20.163039 + rootMeanSquare[indexAy] * -0.66955 + rootMeanSquare[indexGy] * -0.011718 + rootMeanSquare[indexTotalAcc] * -7.293975 + rootMeanSquare[indexPitch] * -0.010334 + skewness[indexAx] * -0.92559 + skewness[indexAy] * 1.177878 + skewness[indexAz] * 0.310915 + skewness[indexGx] * 0.661716 + skewness[indexGy] * 0.454978 + skewness[indexGz] * -0.093635 + skewness[indexTotalAcc] * -0.28927 + skewness[indexTotalGyro] * -1.256266 + skewness[indexRoll] * -0.889538 + skewness[indexPitch] * 0.45993 + kurtosis[indexAx] * 0.07486 + kurtosis[indexAy] * -0.286582 + kurtosis[indexAz] * -0.043846 + kurtosis[indexGx] * -0.073225 + kurtosis[indexGy] * -0.141331 + kurtosis[indexGz] * 0.121389 + kurtosis[indexTotalGyro] * -0.143393 + kurtosis[indexRoll] * 0.105092 + kurtosis[indexPitch] * -0.49702 + meanAbsDev[indexAy] * 4.599369 + meanAbsDev[indexAz] * 25.757985 + meanAbsDev[indexTotalAcc] * -1.221933 + meanAbsDev[indexTotalGyro] * 0.061888 + fftMaxFreq[indexAx] * -0.769628 + fftMaxFreq[indexAy] * 1.113814 + fftMaxFreq[indexGx] * 1.014735 + fftMaxFreq[indexGy] * 0.327943 + fftMaxFreq[indexRoll] * 0.277174 + fftMaxFreq[indexPitch] * -0.204721 + fftEnergy[indexAx] * -6.303551 + fftEnergy[indexGz] * -0.000033 + fftEnergy[indexTotalGyro] * -0.000029 + fftEntropy[indexAy] * 0.04846 + fftEntropy[indexAz] * 0.051672 + fftEntropy[indexGy] * -0.000016 + fftEntropy[indexRoll] * 0.000046 + fftEntropy[indexPitch] * -0.00014 + covarianceGxGy * -0.000341 + covarianceGxGz * -0.000029 + covarianceGyGz * 0.00043 + correlationAxAy * -0.2742 + correlationAyAz * -0.843893 + correlationGxGy * -2.948207 + correlationGyGz * 0.830283 ;
        Class[6] = -7.575126 + standardDeviation[indexAx] * 10.272908 + standardDeviation[indexGy] * 0.037184 + standardDeviation[indexTotalGyro] * -0.006452 + standardDeviation[indexRoll] * 0.103219 + mean[indexGx] * 0.023326 + mean[indexGy] * 0.005318 + mean[indexGz] * 0.078087 + mean[indexRoll] * -0.046614 + mean[indexPitch] * -0.006384 + variance[indexAz] * -3.337393 + variance[indexGz] * -0.000293 + variance[indexTotalAcc] * -1.178948 + variance[indexPitch] * -0.002295 + zeroCrossingRate[indexAz] * 28.506066 + zeroCrossingRate[indexGx] * -18.393169 + zeroCrossingRate[indexGy] * -13.478218 + zeroCrossingRate[indexGz] * 27.506553 + zeroCrossingRate[indexRoll] * -3.046276 + zeroCrossingRate[indexPitch] * -29.739739 + rootMeanSquare[indexAx] * 0.821125 + rootMeanSquare[indexAy] * 2.115288 + rootMeanSquare[indexAz] * -1.729308 + rootMeanSquare[indexGy] * -0.085532 + rootMeanSquare[indexTotalGyro] * -0.003499 + skewness[indexAx] * -0.370168 + skewness[indexAz] * 0.49338 + skewness[indexGx] * 0.849213 + skewness[indexGy] * -0.378922 + skewness[indexGz] * -0.559651 + skewness[indexTotalAcc] * 0.806653 + skewness[indexTotalGyro] * 0.549145 + kurtosis[indexAx] * -0.315546 + kurtosis[indexAy] * 0.279538 + kurtosis[indexAz] * -0.137634 + kurtosis[indexGx] * -1.133302 + kurtosis[indexGy] * -0.134204 + kurtosis[indexGz] * 0.003204 + kurtosis[indexTotalGyro] * 0.516901 + meanAbsDev[indexAy] * 9.132436 + meanAbsDev[indexGx] * 0.062435 + meanAbsDev[indexRoll] * -0.047429 + fftMaxFreq[indexAx] * -0.29363 + fftMaxFreq[indexAy] * -0.521173 + fftMaxFreq[indexGy] * 0.420354 + fftMaxFreq[indexGz] * 0.302924 + fftMaxFreq[indexRoll] * 0.69633 + fftMaxFreq[indexPitch] * -0.737928 + fftEnergy[indexAx] * -8.129035 + fftEnergy[indexGy] * -0.000216 + fftEnergy[indexTotalAcc] * -0.797006 + fftEnergy[indexRoll] * 0.000211 + fftEnergy[indexPitch] * -0.00118 + fftEntropy[indexAx] * 0.063275 + fftEntropy[indexAy] * -0.045995 + fftEntropy[indexAz] * 0.047297 + fftEntropy[indexGx] * -0.00004 + fftEntropy[indexGy] * 0.000087 + fftEntropy[indexGz] * 0.000044 + fftEntropy[indexTotalAcc] * 0.031877 + covarianceAyAz * 43.454431 + covarianceGxGz * -0.000555 + covarianceGyGz * -0.000315 + correlationAxAy * -2.20334 + correlationAyAz * 3.173101 + correlationGxGy * -2.348026 + correlationGxGz * 0.070915 + correlationGyGz * 0.993085 ;
        Class[7] = -12.275108 + standardDeviation[indexGy] * 0.028035 + standardDeviation[indexPitch] * 0.123476 + mean[indexAy] * 1.504042 + mean[indexAz] * 11.309957 + mean[indexGz] * 0.033167 + mean[indexTotalAcc] * -14.65504 + mean[indexRoll] * -0.041154 + variance[indexAx] * -48.344024 + variance[indexAy] * -20.731058 + variance[indexAz] * -9.315226 + variance[indexGz] * -0.000135 + variance[indexTotalAcc] * 2.321599 + variance[indexTotalGyro] * -0.000274 + variance[indexRoll] * -0.000386 + zeroCrossingRate[indexAx] * 13.312236 + zeroCrossingRate[indexAz] * 103.923669 + zeroCrossingRate[indexGx] * 22.153588 + zeroCrossingRate[indexGy] * 2.095528 + zeroCrossingRate[indexGz] * -7.167972 + rootMeanSquare[indexAx] * 7.587781 + rootMeanSquare[indexGy] * -0.007059 + skewness[indexAx] * 0.696662 + skewness[indexAy] * 1.153291 + skewness[indexAz] * -0.113568 + skewness[indexGy] * -0.471099 + skewness[indexGz] * 1.67841 + skewness[indexTotalAcc] * -0.044991 + skewness[indexTotalGyro] * 0.485541 + skewness[indexPitch] * -1.079 + kurtosis[indexAx] * -0.412012 + kurtosis[indexAy] * -0.110424 + kurtosis[indexAz] * -0.216482 + kurtosis[indexGx] * -1.267649 + kurtosis[indexGz] * -0.053878 + kurtosis[indexTotalAcc] * -0.739404 + kurtosis[indexRoll] * -0.114158 + kurtosis[indexPitch] * -0.013721 + meanAbsDev[indexGx] * 0.108142 + meanAbsDev[indexTotalAcc] * 11.299938 + meanAbsDev[indexPitch] * 0.008806 + fftMaxFreq[indexAx] * -0.805062 + fftMaxFreq[indexAy] * 0.477622 + fftMaxFreq[indexGx] * 0.945575 + fftMaxFreq[indexGy] * 0.166545 + fftMaxFreq[indexGz] * -0.256562 + fftMaxFreq[indexRoll] * -0.840915 + fftEnergy[indexAx] * -7.184581 + fftEnergy[indexAy] * -1.70667 + fftEnergy[indexAz] * 1.627574 + fftEnergy[indexGy] * -0.000629 + fftEnergy[indexGz] * -0.000221 + fftEnergy[indexRoll] * -0.000129 + fftEntropy[indexGx] * 0.000027 + fftEntropy[indexGy] * 0.000028 + fftEntropy[indexTotalAcc] * -0.073377 + fftEntropy[indexPitch] * -0.000054 + covarianceAyAz * 20.110731 + correlationAxAy * -0.20607 + correlationAyAz * 15.717645 + correlationGxGy * -1.416913 + correlationGxGz * 1.245706 + correlationGyGz * -1.162137 ;
        Class[8] = -17.976121 + standardDeviation[indexAx] * -1.963251 + standardDeviation[indexAz] * 4.093583 + standardDeviation[indexGy] * -0.035558 + standardDeviation[indexPitch] * -0.045049 + mean[indexAx] * 1.170479 + mean[indexAy] * -7.264734 + mean[indexAz] * -2.964441 + mean[indexGx] * -0.032992 + mean[indexGy] * 0.095756 + mean[indexGz] * 0.011404 + mean[indexRoll] * -0.056312 + variance[indexAy] * 0.397235 + variance[indexGy] * 0.00054 + variance[indexTotalGyro] * -0.000177 + variance[indexRoll] * 0.000949 + zeroCrossingRate[indexAx] * 3.46885 + zeroCrossingRate[indexAz] * 31.770176 + zeroCrossingRate[indexGx] * -23.720061 + zeroCrossingRate[indexGy] * 11.5353 + zeroCrossingRate[indexGz] * 2.860273 + zeroCrossingRate[indexPitch] * 3.967348 + rootMeanSquare[indexAz] * 12.173096 + rootMeanSquare[indexGx] * -0.010199 + rootMeanSquare[indexGy] * -0.019131 + rootMeanSquare[indexGz] * 0.031604 + rootMeanSquare[indexRoll] * 0.011169 + rootMeanSquare[indexPitch] * -0.03865 + skewness[indexAy] * 0.592871 + skewness[indexAz] * 0.364548 + skewness[indexGy] * 0.207897 + skewness[indexTotalAcc] * -0.197922 + kurtosis[indexAx] * 0.105599 + kurtosis[indexAy] * -0.155228 + kurtosis[indexAz] * -0.095993 + kurtosis[indexGx] * 0.071834 + kurtosis[indexGy] * -0.096925 + kurtosis[indexGz] * 0.395281 + kurtosis[indexTotalAcc] * -0.187372 + kurtosis[indexTotalGyro] * -0.434717 + kurtosis[indexRoll] * 0.055272 + kurtosis[indexPitch] * -0.056214 + meanAbsDev[indexAy] * -1.290914 + meanAbsDev[indexAz] * 10.271104 + meanAbsDev[indexGz] * 0.003293 + meanAbsDev[indexTotalAcc] * 10.719889 + meanAbsDev[indexTotalGyro] * 0.034393 + fftMaxFreq[indexGy] * -0.626933 + fftMaxFreq[indexGz] * 0.637031 + fftMaxFreq[indexPitch] * -0.235772 + fftEnergy[indexAx] * -1.627387 + fftEnergy[indexAz] * 0.253067 + fftEnergy[indexGx] * 0.000008 + fftEnergy[indexGy] * 0.000198 + fftEnergy[indexRoll] * -0.00039 + fftEntropy[indexAx] * -0.043252 + fftEntropy[indexAz] * 0.029704 + fftEntropy[indexGz] * -0.000065 + fftEntropy[indexTotalAcc] * -0.028231 + covarianceAxAy * -7.79545 + covarianceAyAz * -14.599646 + covarianceGxGy * 0.000374 + covarianceGyGz * 0.000256 + correlationAxAy * 0.939918 + correlationAyAz * 0.312075 + correlationGxGy * -0.117499 + correlationGxGz * -0.305638 + correlationGyGz * -2.984801 ;
        Class[9] = -11.867415 + standardDeviation[indexTotalGyro] * 0.092044 + standardDeviation[indexPitch] * 0.068765 + mean[indexAx] * 5.460113 + mean[indexAy] * 1.818699 + mean[indexGx] * 0.123494 + mean[indexGy] * -0.16199 + mean[indexGz] * 0.040662 + mean[indexTotalAcc] * -10.84381 + mean[indexRoll] * 0.010353 + mean[indexPitch] * -0.011832 + variance[indexAy] * -15.10398 + variance[indexTotalGyro] * 0.000316 + variance[indexRoll] * -0.000472 + variance[indexPitch] * -0.001496 + zeroCrossingRate[indexAx] * 26.706863 + zeroCrossingRate[indexAy] * 24.305883 + zeroCrossingRate[indexGx] * 6.097188 + zeroCrossingRate[indexGy] * 14.422047 + zeroCrossingRate[indexGz] * -8.209595 + rootMeanSquare[indexAx] * 1.096071 + rootMeanSquare[indexGz] * 0.006281 + skewness[indexAx] * -0.797052 + skewness[indexAy] * 0.704414 + skewness[indexAz] * 0.928205 + skewness[indexGx] * -0.658626 + skewness[indexGy] * -0.907 + skewness[indexGz] * -0.105978 + skewness[indexTotalAcc] * 0.14486 + skewness[indexTotalGyro] * 0.176447 + skewness[indexPitch] * -0.522393 + kurtosis[indexAx] * 0.211912 + kurtosis[indexAz] * -0.841927 + kurtosis[indexGx] * -0.146566 + kurtosis[indexGy] * 0.119012 + kurtosis[indexGz] * 0.219541 + kurtosis[indexTotalAcc] * -0.435282 + kurtosis[indexPitch] * -0.011824 + meanAbsDev[indexAx] * -5.876165 + meanAbsDev[indexGy] * -0.017053 + meanAbsDev[indexGz] * -0.004894 + meanAbsDev[indexTotalAcc] * 16.928855 + meanAbsDev[indexRoll] * -0.028618 + meanAbsDev[indexPitch] * -0.027298 + fftMaxFreq[indexAx] * -1.012543 + fftMaxFreq[indexAy] * -0.787253 + fftMaxFreq[indexGx] * 0.293728 + fftMaxFreq[indexRoll] * -0.981472 + fftEnergy[indexAx] * 10.214635 + fftEnergy[indexAz] * 4.134  + fftEnergy[indexGx] * -0.000062 + fftEnergy[indexPitch] * -0.000948 + fftEntropy[indexAy] * -0.081004 + fftEntropy[indexAz] * 0.117841 + fftEntropy[indexGx] * -0.00003 + fftEntropy[indexGz] * -0.000049 + fftEntropy[indexRoll] * 0.000015 + fftEntropy[indexPitch] * 0.000016 + covarianceGxGy * 0.000411 + covarianceGyGz * 0.000389 + correlationAxAy * -0.504902 + correlationAyAz * -1.134089 + correlationGyGz * -1.704381 ;
        Class[10] = 2.014719 + standardDeviation[indexAx] * -1.722464 + standardDeviation[indexGy] * -0.001687 + standardDeviation[indexGz] * -0.022051 + standardDeviation[indexTotalAcc] * -0.308806 + standardDeviation[indexTotalGyro] * -0.05448 + mean[indexAx] * -1.566824 + mean[indexAz] * -5.409723 + mean[indexGz] * -0.008153 + mean[indexTotalAcc] * 3.571746 + mean[indexTotalGyro] * -0.001389 + mean[indexRoll] * 0.004404 + variance[indexGx] * -0.000208 + variance[indexGy] * 0.000293 + variance[indexTotalGyro] * 0.000677 + zeroCrossingRate[indexAz] * -10.607744 + zeroCrossingRate[indexGz] * 4.766354 + zeroCrossingRate[indexPitch] * -1.44451 + rootMeanSquare[indexAx] * -0.517948 + rootMeanSquare[indexGy] * 0.002789 + rootMeanSquare[indexTotalAcc] * 4.044928 + rootMeanSquare[indexPitch] * 0.025217 + skewness[indexAx] * 0.066782 + skewness[indexGy] * -0.047215 + skewness[indexTotalGyro] * -1.531353 + skewness[indexRoll] * 0.025642 + kurtosis[indexAy] * 0.028006 + kurtosis[indexAz] * 0.309963 + kurtosis[indexGx] * 0.061145 + kurtosis[indexTotalAcc] * 0.302504 + kurtosis[indexTotalGyro] * 0.483154 + meanAbsDev[indexAy] * -3.055131 + meanAbsDev[indexGx] * -0.017695 + meanAbsDev[indexTotalAcc] * -5.091057 + meanAbsDev[indexTotalGyro] * -0.044778 + meanAbsDev[indexPitch] * -0.00434 + fftMaxFreq[indexAx] * 0.295328 + fftMaxFreq[indexAy] * 0.167822 + fftMaxFreq[indexGz] * 0.219252 + fftMaxFreq[indexRoll] * -0.07565 + fftEnergy[indexAx] * 3.434453 + fftEnergy[indexAy] * 1.130532 + fftEnergy[indexAz] * -0.329881 + fftEnergy[indexGx] * 0.000018 + fftEnergy[indexTotalAcc] * 1.650995 + fftEnergy[indexTotalGyro] * 0.000007 + fftEnergy[indexRoll] * 0.000257 + fftEnergy[indexPitch] * 0.000089 + fftEntropy[indexAx] * -0.020721 + fftEntropy[indexAz] * 0.054847 + covarianceGxGz * 0.000033 + covarianceGyGz * -0.00019 + correlationGxGy * 0.59945 + correlationGyGz * 0.125724;


        // MODEL HYBRID
        /*
        Class[0] = -15.598426 +  standardDeviation[indexAx] * 5.9580235 + standardDeviation[indexGx] * 0.0201913 + standardDeviation[indexTotalAcc] * -4.3136132 + standardDeviation[indexRoll] * -0.0119113 + standardDeviation[indexPitch] * 0.0018078 + mean[indexAy] * -0.2282721 + mean[indexAz] * -2.8952946 + mean[indexGx] * -0.0126333 + mean[indexGy] * 0.0160836 + mean[indexGz] * -0.0266072 + mean[indexTotalAcc] * 19.5933245 + mean[indexTotalGyro] * 0.0710799 + mean[indexRoll] * -0.0373972 + mean[indexPitch] * -0.0131075 + variance[indexAz] * -1.0881102 + variance[indexGy] * 0.0005784 + variance[indexGz] * -0.0000851 + variance[indexTotalAcc] * -20.7552333 + variance[indexTotalGyro] * -0.0002742 + variance[indexRoll] * -0.000111 + zeroCrossingRate[indexAx] * 0.4495987 + zeroCrossingRate[indexAz] * -4.9838892 + zeroCrossingRate[indexGx] * 7.2231879 + zeroCrossingRate[indexGy] * -10.1262126 + zeroCrossingRate[indexGz] * 13.7618086 + zeroCrossingRate[indexRoll] * -2.955579 + zeroCrossingRate[indexPitch] * 5.879612  + rootMeanSquare[indexAy] * -1.2067756 + rootMeanSquare[indexAz] * -2.5001747 + rootMeanSquare[indexGz] * -0.0352339 + rootMeanSquare[indexRoll] * 0.0151071 + rootMeanSquare[indexPitch] * 0.029607  + skewness[indexAx] * 0.2280817 + skewness[indexAy] * -0.8822287 + skewness[indexAz] * 0.6820005 + skewness[indexGx] * -0.123425 + skewness[indexGy] * -0.0328618 + skewness[indexGz] * 0.0362942 + skewness[indexTotalAcc] * 0.4062401 + skewness[indexTotalGyro] * -0.1407557 + skewness[indexRoll] * 0.0578469 + skewness[indexPitch] * 0.1489909 + kurtosis[indexAx] * 0.0616936 + kurtosis[indexGx] * 0.1026289 + kurtosis[indexGy] * -0.0792794 + kurtosis[indexGz] * -0.0864794 + kurtosis[indexTotalGyro] * 0.3053229 + kurtosis[indexRoll] * 0.0705695 + meanAbsDev[indexAx] * -7.229595 + meanAbsDev[indexAy] * 3.3798085 + meanAbsDev[indexAz] * 7.0783782 + meanAbsDev[indexGy] * -0.1898339 + meanAbsDev[indexGz] * -0.0966532 + meanAbsDev[indexRoll] * 0.006936  + fftMaxFreq[indexAx] * 0.1298348 + fftMaxFreq[indexAy] * 0.4032172 + fftMaxFreq[indexGx] * -0.302039 + fftMaxFreq[indexGy] * 0.0743736 + fftMaxFreq[indexGz] * -0.506535 + fftMaxFreq[indexRoll] * 0.1437817 + fftMaxFreq[indexPitch] * 0.0515698 + fftEnergy[indexAx] * -8.81755 + fftEnergy[indexAy] * 0.4411592 + fftEnergy[indexAz] * -0.437363 + fftEnergy[indexGy] * -0.0004918 + fftEnergy[indexGz] * -0.0004255 + fftEnergy[indexRoll] * 0.0000588 + fftEnergy[indexPitch] * 0.0001096 + fftEntropy[indexAy] * 0.0697801 + fftEntropy[indexGz] * 0.0000587 + fftEntropy[indexTotalGyro] * -0.0000234 + covarianceAxAy * 4.6674295 + covarianceAyAz * -4.5358767 + covarianceGxGy * -0.0004 + covarianceGxGz * -0.0001292 + covarianceGyGz * 0.0008665 + correlationAxAy * 0.4916996 + correlationAyAz * 1.1721983 + correlationGxGy * -0.3016461 + correlationGxGz * 0.2181811 + correlationGyGz * -0.7704314 ;
        Class[1] = 25.8540629 +  standardDeviation[indexAx] * -6.2656413 + standardDeviation[indexAy] * 2.0599847 + standardDeviation[indexPitch] * 0.0363094 + mean[indexAx] * 3.4369084 + mean[indexAy] * 4.9128115 + mean[indexAz] * -2.9881349 + mean[indexGx] * 0.0158576 + mean[indexGy] * 0.0128866 + mean[indexGz] * -0.0233488 + mean[indexTotalAcc] * -12.5957377 + mean[indexTotalGyro] * -0.0029621 + mean[indexPitch] * -0.0031872 + variance[indexAx] * 11.4036886 + variance[indexGx] * 0.0000764 + variance[indexGy] * 0.0008863 + variance[indexTotalAcc] * 10.8566947 + variance[indexTotalGyro] * 0.0000489 + zeroCrossingRate[indexAz] * 9.2460451 + zeroCrossingRate[indexGy] * -17.1058052 + zeroCrossingRate[indexGz] * 28.2587117 + zeroCrossingRate[indexRoll] * -2.8941141 + rootMeanSquare[indexAx] * -5.0311957 + rootMeanSquare[indexAz] * -9.6202147 + rootMeanSquare[indexGz] * -0.1308846 + rootMeanSquare[indexRoll] * -0.0153779 + skewness[indexAx] * -0.1428762 + skewness[indexAy] * 1.4089929 + skewness[indexAz] * 0.0930557 + skewness[indexGx] * -1.0076747 + skewness[indexGy] * -0.4864276 + skewness[indexGz] * -0.2677997 + skewness[indexTotalAcc] * -0.4867899 + skewness[indexRoll] * 0.3534812 + skewness[indexPitch] * 1.1040664 + kurtosis[indexAx] * 0.0328828 + kurtosis[indexAy] * 0.0686453 + kurtosis[indexAz] * 0.0565183 + kurtosis[indexGx] * -0.5845752 + kurtosis[indexGy] * -0.0412929 + kurtosis[indexGz] * -0.0945292 + kurtosis[indexTotalAcc] * -0.2491773 + kurtosis[indexTotalGyro] * -0.4645077 + kurtosis[indexRoll] * -0.0189165 + kurtosis[indexPitch] * -0.7969014 + meanAbsDev[indexAy] * 10.8542063 + meanAbsDev[indexGx] * 0.039883  + meanAbsDev[indexGy] * -0.0028145 + meanAbsDev[indexGz] * -0.1541755 + meanAbsDev[indexRoll] * -0.0251822 + fftMaxFreq[indexAx] * -0.4436042 + fftMaxFreq[indexAy] * 0.7364371 + fftMaxFreq[indexGx] * 0.1978796 + fftMaxFreq[indexGy] * -0.1273957 + fftMaxFreq[indexGz] * -0.6314606 + fftMaxFreq[indexRoll] * 1.238839  + fftMaxFreq[indexPitch] * -1.4559132 + fftEnergy[indexAz] * -1.1232464 + fftEnergy[indexGz] * 0.000184  + fftEnergy[indexTotalAcc] * -0.358423 + fftEnergy[indexRoll] * -0.0005752 + fftEnergy[indexPitch] * -0.0002332 + fftEntropy[indexAy] * 0.0093808 + fftEntropy[indexAz] * -0.0728727 + fftEntropy[indexGx] * 0.0000185 + fftEntropy[indexGz] * -0.000015 + fftEntropy[indexTotalAcc] * 0.0267924 + fftEntropy[indexRoll] * -0.0000171 + covarianceGxGz * -0.0005877 + covarianceGyGz * -0.0009516 + correlationAxAy * -0.6699001 + correlationAyAz * -2.9262225 + correlationGxGy * -2.7612324 + correlationGxGz * -2.286306 + correlationGyGz * -0.2559535 ;
        Class[2] = -22.0159245 +  standardDeviation[indexAx] * 0.2969596 + standardDeviation[indexGy] * 0.0571393 + standardDeviation[indexTotalAcc] * -6.8035778 + mean[indexAx] * -6.8540127 + mean[indexGx] * -0.0238455 + mean[indexGy] * -0.0799805 + mean[indexGz] * 0.0045401 + mean[indexTotalAcc] * 6.2824058 + mean[indexRoll] * 0.0158086 + mean[indexPitch] * 0.1779455 + variance[indexAx] * 1.0887006 + variance[indexAy] * 8.1559958 + variance[indexAz] * 2.9630568 + variance[indexGz] * 0.0003556 + variance[indexTotalAcc] * 7.8360455 + variance[indexTotalGyro] * 0.000277  + variance[indexRoll] * -0.0005754 + variance[indexPitch] * 0.0003192 + zeroCrossingRate[indexAz] * 60.3954425 + zeroCrossingRate[indexGx] * 15.2967717 + zeroCrossingRate[indexGy] * 8.771292  + rootMeanSquare[indexAy] * -12.1499634 + rootMeanSquare[indexAz] * 1.4460501 + rootMeanSquare[indexGz] * 0.0317758 + rootMeanSquare[indexTotalAcc] * 8.0639843 + skewness[indexAx] * -0.7817977 + skewness[indexAy] * -0.7460279 + skewness[indexAz] * 0.5614692 + skewness[indexGx] * -0.1065539 + skewness[indexGy] * -0.050946 + skewness[indexGz] * 0.5540742 + skewness[indexTotalAcc] * 0.0944901 + skewness[indexTotalGyro] * 0.1617676 + skewness[indexRoll] * 0.2153004 + skewness[indexPitch] * -0.4204279 + kurtosis[indexAx] * -0.0250668 + kurtosis[indexGx] * 0.290964  + kurtosis[indexGy] * 0.1752807 + kurtosis[indexGz] * -0.8432821 + kurtosis[indexTotalAcc] * 0.2713571 + kurtosis[indexTotalGyro] * -0.339728 + kurtosis[indexRoll] * -0.305348 + meanAbsDev[indexTotalAcc] * -51.034248 + meanAbsDev[indexRoll] * -0.0169945 + meanAbsDev[indexPitch] * 0.0483759 + fftMaxFreq[indexAx] * 0.6214088 + fftMaxFreq[indexAy] * -1.0295982 + fftMaxFreq[indexAz] * -23.6605787 + fftMaxFreq[indexGx] * -0.4388904 + fftMaxFreq[indexGz] * 0.558309  + fftMaxFreq[indexPitch] * -1.1076182 + fftEnergy[indexAy] * -2.3614912 + fftEnergy[indexGy] * -0.0004729 + fftEnergy[indexGz] * -0.0000132 + fftEnergy[indexTotalAcc] * 0.7615489 + fftEnergy[indexTotalGyro] * -0.0000289 + fftEntropy[indexAx] * 0.0372756 + fftEntropy[indexAy] * 0.0591832 + fftEntropy[indexAz] * -0.1173223 + fftEntropy[indexGx] * 0.0000448 + fftEntropy[indexGz] * -0.000015 + fftEntropy[indexTotalAcc] * -0.0852562 + fftEntropy[indexRoll] * 0.0000235 + fftEntropy[indexPitch] * -0.0002367 + covarianceAxAy * -19.378612 + covarianceGxGy * 0.0005961 + covarianceGxGz * 0.0002937 + covarianceGyGz * -0.0001191 + correlationAxAy * 0.2455093 + correlationAyAz * -0.4727821 + correlationGxGy * -0.2573556 + correlationGxGz * 0.8369548 + correlationGyGz * 1.7668571 ;
        Class[3] = -31.5818566 +  standardDeviation[indexAy] * -3.0775969 + standardDeviation[indexAz] * 1.1247082 + standardDeviation[indexGy] * 0.0038463 + standardDeviation[indexGz] * 0.1240698 + standardDeviation[indexTotalAcc] * -9.288149 + standardDeviation[indexRoll] * 0.0166377 + mean[indexAx] * 2.3478708 + mean[indexGx] * -0.0252101 + mean[indexGz] * -0.0058038 + mean[indexTotalAcc] * 28.1286238 + variance[indexAx] * -1.8922033 + variance[indexAy] * -25.4579453 + variance[indexGy] * -0.0000733 + variance[indexGz] * -0.0003208 + variance[indexTotalAcc] * -2.9538261 + variance[indexTotalGyro] * -0.0007658 + zeroCrossingRate[indexAx] * 3.0932312 + zeroCrossingRate[indexAy] * 5.8354717 + zeroCrossingRate[indexAz] * 17.0755318 + zeroCrossingRate[indexGx] * 7.0606947 + zeroCrossingRate[indexGy] * 3.9629755 + zeroCrossingRate[indexGz] * -7.6909667 + rootMeanSquare[indexAy] * 1.7600238 + rootMeanSquare[indexGx] * -0.0165743 + rootMeanSquare[indexPitch] * 0.0762858 + skewness[indexAx] * 2.0348647 + skewness[indexAy] * 1.0170962 + skewness[indexAz] * -1.2492144 + skewness[indexGx] * 0.3041385 + skewness[indexGy] * -0.0995317 + skewness[indexGz] * -1.3857167 + skewness[indexTotalAcc] * 0.1134985 + skewness[indexRoll] * -1.1871059 + kurtosis[indexAx] * -0.4671588 + kurtosis[indexAy] * -0.4236242 + kurtosis[indexGx] * -0.064115 + kurtosis[indexGy] * 0.3304876 + kurtosis[indexGz] * -0.5468417 + kurtosis[indexTotalAcc] * 0.1856795 + kurtosis[indexTotalGyro] * -0.1280136 + kurtosis[indexRoll] * -0.4762639 + kurtosis[indexPitch] * -0.1937105 + meanAbsDev[indexAx] * 2.335367  + meanAbsDev[indexAy] * -3.8755095 + meanAbsDev[indexGz] * 0.0563604 + meanAbsDev[indexTotalAcc] * -6.2316356 + fftMaxFreq[indexAz] * -9.0384806 + fftMaxFreq[indexGx] * -0.3262307 + fftMaxFreq[indexGy] * 0.188152  + fftMaxFreq[indexGz] * -0.7164714 + fftMaxFreq[indexPitch] * 0.8714171 + fftEnergy[indexAy] * -4.6363452 + fftEnergy[indexGx] * -0.0000218 + fftEnergy[indexGy] * -0.0009952 + fftEnergy[indexGz] * -0.0000149 + fftEnergy[indexTotalGyro] * -0.0000667 + fftEntropy[indexAx] * 0.0164681 + fftEntropy[indexAy] * -0.0266457 + fftEntropy[indexAz] * 0.0374957 + fftEntropy[indexGx] * 0.0000306 + fftEntropy[indexGz] * -0.0000983 + fftEntropy[indexTotalAcc] * -0.0232644 + fftEntropy[indexRoll] * 0.0000229 + fftEntropy[indexPitch] * -0.0000134 + covarianceAxAy * -42.8360716 + covarianceGxGy * -0.0019061 + covarianceGyGz * -0.0001048 + correlationAxAy * -1.2636063 + correlationAyAz * 1.3092473 + correlationGxGy * 1.1830716 + correlationGxGz * -2.1938914 + correlationGyGz * -1.5926586 ;
        Class[4] = 7.1380601 +  standardDeviation[indexAx] * 11.3028229 + standardDeviation[indexAy] * 6.7466181 + standardDeviation[indexGx] * 0.0493657 + standardDeviation[indexTotalAcc] * 0.5010996 + standardDeviation[indexRoll] * 0.0510707 + mean[indexAy] * -1.2033421 + mean[indexAz] * 6.2395369 + mean[indexGy] * -0.1378836 + mean[indexGz] * -0.0347949 + mean[indexTotalAcc] * -11.2503297 + mean[indexRoll] * -0.1433725 + variance[indexGy] * -0.0001578 + variance[indexGz] * -0.0007467 + variance[indexTotalGyro] * -0.0001813 + variance[indexPitch] * -0.0006782 + zeroCrossingRate[indexAy] * 9.8832345 + zeroCrossingRate[indexAz] * -77.4520838 + zeroCrossingRate[indexGx] * -3.8979096 + zeroCrossingRate[indexGy] * -15.3853501 + zeroCrossingRate[indexGz] * -8.4132108 + zeroCrossingRate[indexRoll] * -0.8674027 + zeroCrossingRate[indexPitch] * -3.2511369 + rootMeanSquare[indexAx] * -3.672272 + rootMeanSquare[indexGy] * -0.0160033 + skewness[indexAx] * 0.6984853 + skewness[indexAy] * 1.4354133 + skewness[indexAz] * 0.1504027 + skewness[indexGx] * -0.2572101 + skewness[indexTotalAcc] * 0.9962442 + skewness[indexTotalGyro] * 1.0841185 + skewness[indexPitch] * 0.6114709 + kurtosis[indexAx] * -0.0286916 + kurtosis[indexAy] * -0.7098161 + kurtosis[indexAz] * -0.7162253 + kurtosis[indexGx] * -0.5533688 + kurtosis[indexGy] * -0.0171282 + kurtosis[indexGz] * -0.0941366 + kurtosis[indexTotalGyro] * 0.0290744 + kurtosis[indexRoll] * -0.0546759 + kurtosis[indexPitch] * -0.0478482 + meanAbsDev[indexAx] * 2.4344832 + meanAbsDev[indexAy] * 11.3934999 + meanAbsDev[indexGx] * -0.0016906 + meanAbsDev[indexGy] * -0.0177003 + meanAbsDev[indexTotalAcc] * 0.3287761 + meanAbsDev[indexTotalGyro] * -0.0684123 + meanAbsDev[indexRoll] * -0.0035175 + meanAbsDev[indexPitch] * -0.0883156 + fftMaxFreq[indexAy] * 0.7672226 + fftMaxFreq[indexGx] * 0.4971118 + fftMaxFreq[indexGz] * -0.3828521 + fftMaxFreq[indexPitch] * -0.0963605 + fftEnergy[indexAx] * 0.9687317 + fftEnergy[indexAy] * -0.1518778 + fftEnergy[indexGz] * -0.0001139 + fftEnergy[indexPitch] * -0.0001504 + fftEntropy[indexAx] * -0.0246265 + fftEntropy[indexAy] * -0.0180423 + fftEntropy[indexGx] * -0.0000086 + fftEntropy[indexGz] * -0.0000362 + fftEntropy[indexTotalAcc] * 0.0182241 + fftEntropy[indexTotalGyro] * -0.0000561 + covarianceAxAy * 13.9251798 + covarianceAyAz * 2.2571511 + covarianceGxGy * -0.0000251 + covarianceGxGz * 0.0000926 + covarianceGyGz * -0.0008241 + correlationAxAy * -0.9172171 + correlationAyAz * -0.9994486 + correlationGxGy * -0.2092442 + correlationGxGz * 0.3329284 + correlationGyGz * 2.1809356 ;
        Class[5] = 5.9846301 +  standardDeviation[indexTotalGyro] * -0.0135876 + mean[indexAy] * -0.3437246 + mean[indexAz] * -2.4908049 + mean[indexGx] * 0.0051095 + mean[indexGy] * 0.060625  + mean[indexGz] * 0.0183793 + mean[indexTotalAcc] * -5.6596987 + mean[indexRoll] * -0.0022942 + mean[indexPitch] * -0.0232937 + variance[indexAx] * 2.8306511 + variance[indexAy] * -8.519065 + variance[indexAz] * 1.4979446 + variance[indexGy] * 0.0007555 + variance[indexGz] * -0.0001995 + variance[indexTotalAcc] * 6.5102215 + variance[indexTotalGyro] * -0.0001862 + variance[indexRoll] * -0.0008868 + variance[indexPitch] * -0.000915 + zeroCrossingRate[indexAx] * 3.2220134 + zeroCrossingRate[indexAz] * 45.1342065 + zeroCrossingRate[indexGx] * -3.9764546 + zeroCrossingRate[indexGy] * 1.4734153 + zeroCrossingRate[indexGz] * -18.3493419 + zeroCrossingRate[indexPitch] * -12.5261968 + rootMeanSquare[indexAy] * -0.228807 + rootMeanSquare[indexGy] * -0.0567458 + rootMeanSquare[indexPitch] * 0.0256042 + skewness[indexAx] * -0.1591485 + skewness[indexAy] * -0.1811704 + skewness[indexAz] * 0.301924  + skewness[indexGy] * 0.2566955 + skewness[indexTotalAcc] * 0.3439228 + skewness[indexRoll] * -0.0556251 + skewness[indexPitch] * 0.4646152 + kurtosis[indexAx] * -0.0130831 + kurtosis[indexAy] * -0.1279455 + kurtosis[indexAz] * -0.2844787 + kurtosis[indexGx] * 0.0877115 + kurtosis[indexGy] * -0.013565 + kurtosis[indexGz] * 0.0651225 + kurtosis[indexTotalAcc] * -0.0742268 + kurtosis[indexTotalGyro] * 0.3919482 + kurtosis[indexRoll] * 0.058971  + kurtosis[indexPitch] * 0.0159093 + meanAbsDev[indexAx] * 7.7736622 + meanAbsDev[indexAy] * 18.4932086 + meanAbsDev[indexAz] * 1.8085702 + meanAbsDev[indexGy] * -0.0190482 + meanAbsDev[indexTotalGyro] * -0.031837 + fftMaxFreq[indexAx] * -0.3382613 + fftMaxFreq[indexAy] * 0.3944126 + fftMaxFreq[indexAz] * -3.7746171 + fftMaxFreq[indexGx] * -0.1504382 + fftMaxFreq[indexGy] * 0.0628649 + fftMaxFreq[indexGz] * 0.1524093 + fftMaxFreq[indexRoll] * 0.4458484 + fftEnergy[indexAy] * 0.2441034 + fftEnergy[indexAz] * 0.5868061 + fftEnergy[indexGy] * -0.0000163 + fftEnergy[indexGz] * -0.0001198 + fftEnergy[indexRoll] * -0.0002139 + fftEntropy[indexAx] * 0.0051812 + fftEntropy[indexGy] * 0.0000544 + fftEntropy[indexTotalAcc] * -0.0097627 + fftEntropy[indexTotalGyro] * -0.0000155 + fftEntropy[indexPitch] * -0.0000783 + covarianceAyAz * 4.8220791 + covarianceGxGy * 0.0001649 + covarianceGyGz * 0.0005068 + correlationAyAz * -2.7831023 + correlationGxGy * 0.1644507 + correlationGxGz * 0.5981879 + correlationGyGz * 0.4726797 ;
        Class[6] = 1.1707212 +  standardDeviation[indexGy] * 0.0134423 + standardDeviation[indexTotalGyro] * -0.0153875 + mean[indexAx] * -2.0503787 + mean[indexAy] * 1.3851902 + mean[indexAz] * 4.7775958 + mean[indexGx] * 0.0127066 + mean[indexGy] * 0.0663977 + mean[indexGz] * 0.0430763 + mean[indexTotalAcc] * -0.9183237 + mean[indexRoll] * 0.0769534 + mean[indexPitch] * 0.0357848 + variance[indexAx] * 6.0726331 + variance[indexAy] * -3.0587043 + variance[indexGx] * -0.0000653 + variance[indexGz] * 0.0001644 + variance[indexTotalAcc] * 3.7345913 + zeroCrossingRate[indexAx] * -2.9375776 + zeroCrossingRate[indexAy] * -5.1952282 + zeroCrossingRate[indexAz] * 13.3996067 + zeroCrossingRate[indexGx] * -16.2157248 + zeroCrossingRate[indexGy] * 2.2181736 + zeroCrossingRate[indexGz] * -5.5375301 + rootMeanSquare[indexAx] * 1.705752  + rootMeanSquare[indexAy] * -2.1983786 + rootMeanSquare[indexAz] * 0.3790884 + rootMeanSquare[indexGy] * -0.0108572 + rootMeanSquare[indexRoll] * -0.0018273 + skewness[indexAx] * 0.6461521 + skewness[indexAy] * 1.2895697 + skewness[indexAz] * 0.6840399 + skewness[indexGx] * -0.6751322 + skewness[indexGy] * 0.1559917 + skewness[indexGz] * -0.1966632 + skewness[indexTotalAcc] * -0.250276 + skewness[indexTotalGyro] * 1.3529894 + skewness[indexRoll] * 0.2073806 + kurtosis[indexAx] * 0.2702827 + kurtosis[indexAy] * -0.325343 + kurtosis[indexAz] * -0.0715272 + kurtosis[indexGx] * -1.4809032 + kurtosis[indexGy] * 0.2565292 + kurtosis[indexGz] * 0.0695412 + kurtosis[indexTotalAcc] * -0.6958267 + kurtosis[indexTotalGyro] * -1.0781643 + kurtosis[indexRoll] * -0.1916946 + kurtosis[indexPitch] * 0.1492688 + meanAbsDev[indexAx] * -14.4276397 + meanAbsDev[indexAy] * -0.6371643 + meanAbsDev[indexAz] * -5.3845687 + meanAbsDev[indexGx] * 0.0338439 + meanAbsDev[indexGy] * 0.0799606 + meanAbsDev[indexGz] * -0.0100882 + meanAbsDev[indexRoll] * -0.004507 + meanAbsDev[indexPitch] * 0.0937956 + fftMaxFreq[indexAy] * -1.0418299 + fftMaxFreq[indexAz] * -2.6165586 + fftMaxFreq[indexGx] * 0.2612781 + fftMaxFreq[indexGz] * -0.4804439 + fftMaxFreq[indexPitch] * 0.483058  + fftEnergy[indexAx] * 2.8077029 + fftEnergy[indexGx] * 0.0000192 + fftEntropy[indexAx] * -0.0205691 + fftEntropy[indexAy] * -0.0207916 + fftEntropy[indexAz] * -0.0246047 + fftEntropy[indexGy] * 0.0000482 + fftEntropy[indexGz] * -0.0000064 + fftEntropy[indexTotalGyro] * 0.0000688 + fftEntropy[indexRoll] * -0.0000056 + fftEntropy[indexPitch] * 0.0000167 + covarianceAxAy * -19.846606 + covarianceAyAz * -3.340486 + covarianceGxGy * 0.0000491 + covarianceGxGz * -0.0002625 + covarianceGyGz * -0.0006814 + correlationAxAy * -0.4059096 + correlationAyAz * 4.7195803 + correlationGxGy * 0.1747326 + correlationGxGz * 0.2396246 + correlationGyGz * 1.124836  ;
        Class[7] = 0.344533  +  standardDeviation[indexGz] * 0.0044119 + standardDeviation[indexTotalAcc] * 11.0495622 + standardDeviation[indexRoll] * 0.0146885 + mean[indexAx] * -1.9820416 + mean[indexAy] * 2.3247628 + mean[indexAz] * 0.5720967 + mean[indexGx] * -0.0202881 + mean[indexGy] * -0.0799367 + mean[indexTotalAcc] * -4.322282 + mean[indexRoll] * 0.0521112 + mean[indexPitch] * -0.0019924 + variance[indexAy] * -19.2778879 + variance[indexAz] * -0.9453512 + variance[indexGy] * 0.0007683 + variance[indexRoll] * 0.0000785 + variance[indexPitch] * 0.0001744 + zeroCrossingRate[indexAx] * 2.052557  + zeroCrossingRate[indexAy] * 1.7460493 + zeroCrossingRate[indexAz] * -16.8631055 + zeroCrossingRate[indexGx] * -3.997746 + zeroCrossingRate[indexGy] * 10.1423726 + zeroCrossingRate[indexPitch] * -6.0152411 + rootMeanSquare[indexAx] * -3.016125 + rootMeanSquare[indexAy] * 1.0583918 + rootMeanSquare[indexAz] * -0.7319379 + rootMeanSquare[indexGy] * -0.0998856 + rootMeanSquare[indexRoll] * 0.0205104 + rootMeanSquare[indexPitch] * 0.0061413 + skewness[indexAx] * 0.087183  + skewness[indexAy] * -0.9070963 + skewness[indexAz] * 1.477661  + skewness[indexGx] * 0.2186209 + skewness[indexGy] * -0.3389792 + skewness[indexGz] * 0.1569303 + skewness[indexTotalAcc] * -1.1134305 + skewness[indexTotalGyro] * 0.8071988 + skewness[indexRoll] * 0.8911526 + skewness[indexPitch] * -0.1070628 + kurtosis[indexAx] * -0.1403805 + kurtosis[indexGx] * -0.3236547 + kurtosis[indexGy] * 0.1201169 + kurtosis[indexGz] * -0.5016803 + kurtosis[indexTotalGyro] * 0.1505761 + kurtosis[indexRoll] * 0.058221  + kurtosis[indexPitch] * -0.014736 + meanAbsDev[indexAy] * 9.3297538 + meanAbsDev[indexGx] * 0.0028271 + meanAbsDev[indexGz] * 0.0083456 + meanAbsDev[indexTotalAcc] * 5.5953974 + meanAbsDev[indexPitch] * 0.0332209 + fftMaxFreq[indexAx] * -0.3496627 + fftMaxFreq[indexAz] * -0.7926273 + fftMaxFreq[indexGx] * 1.1200871 + fftMaxFreq[indexGy] * 0.0745796 + fftMaxFreq[indexRoll] * 0.2671835 + fftMaxFreq[indexPitch] * 0.7231765 + fftEnergy[indexAx] * -2.1506643 + fftEnergy[indexGx] * -0.000029 + fftEnergy[indexGy] * -0.0001435 + fftEnergy[indexGz] * 0.0000672 + fftEnergy[indexPitch] * -0.000033 + fftEntropy[indexAx] * -0.0859137 + fftEntropy[indexAy] * -0.0130183 + fftEntropy[indexGx] * 0.0000121 + fftEntropy[indexTotalAcc] * -0.0164681 + fftEntropy[indexTotalGyro] * 0.0000287 + fftEntropy[indexRoll] * 0.000009  + covarianceAxAy * -16.7845766 + covarianceGxGy * -0.0002584 + covarianceGyGz * 0.0000632 + correlationAxAy * 1.9234981 + correlationAyAz * -0.9910271 + correlationGxGy * 1.222219  + correlationGxGz * 1.1324728 + correlationGyGz * -0.9881641 ;
        Class[8] = -7.2619109 +  standardDeviation[indexAx] * 16.4834448 + standardDeviation[indexGy] * -0.0757188 + mean[indexAx] * 2.4427731 + mean[indexAz] * 3.5184661 + mean[indexGx] * 0.0155749 + mean[indexGz] * 0.0038277 + mean[indexTotalAcc] * 0.3653482 + mean[indexRoll] * -0.0171824 + mean[indexPitch] * -0.004833 + variance[indexAx] * -7.1134065 + variance[indexAy] * 9.0680507 + variance[indexAz] * -3.2501568 + variance[indexGy] * -0.0000959 + variance[indexGz] * -0.0001778 + variance[indexTotalAcc] * -5.9622511 + variance[indexRoll] * 0.0000226 + zeroCrossingRate[indexAz] * 18.9391131 + zeroCrossingRate[indexGx] * 15.1800644 + zeroCrossingRate[indexGy] * 1.1724722 + zeroCrossingRate[indexGz] * 5.5771547 + zeroCrossingRate[indexRoll] * -1.9986764 + zeroCrossingRate[indexPitch] * -28.8417453 + rootMeanSquare[indexGx] * 0.0109381 + rootMeanSquare[indexGy] * 0.0917093 + skewness[indexAx] * 0.6608406 + skewness[indexAy] * 0.1801878 + skewness[indexGx] * 0.5400296 + skewness[indexGy] * 1.7699066 + skewness[indexGz] * -0.2797489 + skewness[indexTotalAcc] * -0.2412534 + skewness[indexTotalGyro] * -1.704899 + skewness[indexRoll] * -0.2906282 + skewness[indexPitch] * -0.250248 + kurtosis[indexAx] * 0.0860309 + kurtosis[indexAy] * 0.3965436 + kurtosis[indexGx] * -0.4434147 + kurtosis[indexGy] * -0.0931591 + kurtosis[indexGz] * 0.1040128 + kurtosis[indexTotalAcc] * -0.4375286 + kurtosis[indexTotalGyro] * 0.0496571 + kurtosis[indexRoll] * -0.0752777 + kurtosis[indexPitch] * -0.132484 + meanAbsDev[indexAy] * -11.5768608 + meanAbsDev[indexAz] * 0.2804095 + meanAbsDev[indexTotalGyro] * 0.0156053 + meanAbsDev[indexPitch] * -0.0220661 + fftMaxFreq[indexAx] * 0.1959622 + fftMaxFreq[indexAy] * 0.9502032 + fftMaxFreq[indexAz] * 0.3939623 + fftMaxFreq[indexGx] * -0.3905231 + fftMaxFreq[indexGy] * -0.1604019 + fftMaxFreq[indexGz] * -0.1598701 + fftMaxFreq[indexRoll] * 0.4513818 + fftMaxFreq[indexPitch] * 0.281824  + fftEnergy[indexAx] * -0.2231344 + fftEnergy[indexAy] * 1.7225526 + fftEnergy[indexGy] * 0.0000988 + fftEnergy[indexPitch] * 0.0000183 + fftEntropy[indexAx] * 0.039075  + fftEntropy[indexAy] * 0.0034576 + fftEntropy[indexGx] * -0.0000075 + fftEntropy[indexGy] * -0.0000053 + fftEntropy[indexTotalGyro] * 0.0000147 + fftEntropy[indexRoll] * 0.0000052 + covarianceAxAy * 7.1812717 + covarianceAyAz * -1.1218295 + covarianceGxGy * 0.0003631 + covarianceGyGz * 0.0002769 + correlationAxAy * 0.5778039 + correlationAyAz * 2.4713732 + correlationGxGy * -2.1462532 + correlationGxGz * 0.5230333 + correlationGyGz * 0.3370052 ;
        Class[9] = -0.3079567 +  standardDeviation[indexGy] * -0.0199548 + standardDeviation[indexGz] * 0.0152948 + mean[indexAx] * -1.3469494 + mean[indexAy] * 0.2971895 + mean[indexGx] * -0.0058741 + mean[indexGz] * 0.0135767 + mean[indexRoll] * -0.0142029 + mean[indexPitch] * 0.0031996 + variance[indexAx] * 10.184372 + variance[indexAy] * 0.207262  + variance[indexGx] * 0.0000199 + variance[indexGy] * -0.0000933 + variance[indexTotalAcc] * -0.2872849 + variance[indexRoll] * 0.0004353 + variance[indexPitch] * 0.0003901 + zeroCrossingRate[indexAx] * 3.7885238 + zeroCrossingRate[indexAz] * -15.2936012 + zeroCrossingRate[indexGx] * -4.8463709 + zeroCrossingRate[indexGy] * -11.0117291 + zeroCrossingRate[indexGz] * 8.8161233 + zeroCrossingRate[indexRoll] * -0.9459116 + rootMeanSquare[indexAx] * -2.8576608 + rootMeanSquare[indexAy] * -0.3073675 + rootMeanSquare[indexGy] * 0.0919757 + rootMeanSquare[indexTotalAcc] * -1.4075501 + skewness[indexAx] * -1.0210861 + skewness[indexAy] * 0.246526  + skewness[indexAz] * -0.2169593 + skewness[indexGx] * -0.2806841 + skewness[indexGy] * -1.1061378 + skewness[indexGz] * 0.2331758 + skewness[indexTotalAcc] * 0.1956823 + skewness[indexTotalGyro] * -1.8296982 + skewness[indexRoll] * -0.2863913 + skewness[indexPitch] * -0.3908896 + kurtosis[indexAx] * -0.033378 + kurtosis[indexAy] * 0.1668182 + kurtosis[indexAz] * -0.1606332 + kurtosis[indexGx] * 0.0725756 + kurtosis[indexGy] * -0.0805615 + kurtosis[indexGz] * 0.2720533 + kurtosis[indexTotalAcc] * -0.038003 + kurtosis[indexRoll] * -0.0391266 + meanAbsDev[indexAx] * -5.4147852 + meanAbsDev[indexGy] * -0.0510822 + meanAbsDev[indexGz] * -0.0020007 + meanAbsDev[indexTotalAcc] * -6.2896152 + meanAbsDev[indexTotalGyro] * 0.0544223 + meanAbsDev[indexRoll] * -0.0795453 + meanAbsDev[indexPitch] * -0.1642572 + fftMaxFreq[indexAx] * 0.3116561 + fftMaxFreq[indexAy] * -0.3725945 + fftMaxFreq[indexGy] * -0.1036657 + fftMaxFreq[indexRoll] * 0.2981763 + fftMaxFreq[indexPitch] * 0.455039  + fftEnergy[indexAx] * 0.635633  + fftEnergy[indexAz] * -1.083912 + fftEnergy[indexGy] * 0.0001378 + fftEntropy[indexAx] * -0.026982 + fftEntropy[indexAz] * -0.0329732 + fftEntropy[indexGx] * -0.0000168 + fftEntropy[indexGy] * 0.0000264 + fftEntropy[indexGz] * -0.0000119 + fftEntropy[indexTotalGyro] * 0.0000026 + covarianceAxAy * 5.8205868 + covarianceAyAz * 9.378583  + covarianceGxGy * -0.0002922 + covarianceGxGz * -0.0000605 + covarianceGyGz * 0.0000652 + correlationAxAy * -1.2186405 + correlationAyAz * 2.7198742 + correlationGxGy * 4.955189  + correlationGxGz * -1.3286735 + correlationGyGz * 1.7834077 ;
        Class[10] = 3.7911795 +  standardDeviation[indexAx] * -7.4426855 + standardDeviation[indexGx] * -0.0279106 + standardDeviation[indexGz] * -0.0485669 + standardDeviation[indexTotalGyro] * 0.0010903 + standardDeviation[indexPitch] * -0.01245 + mean[indexAx] * 0.7679049 + mean[indexAy] * -1.6858319 + mean[indexAz] * -6.6116297 + mean[indexGx] * -0.0036679 + mean[indexGy] * -0.0059398 + mean[indexGz] * -0.0058888 + mean[indexTotalAcc] * 11.4675769 + mean[indexRoll] * -0.0082664 + variance[indexAx] * -0.7393266 + variance[indexAy] * 2.4122454 + variance[indexAz] * 0.1842813 + variance[indexGx] * 0.0000818 + variance[indexGy] * 0.0003301 + variance[indexRoll] * -0.000487 + zeroCrossingRate[indexAx] * 1.0745261 + zeroCrossingRate[indexAy] * 6.317508  + zeroCrossingRate[indexAz] * -6.6785203 + zeroCrossingRate[indexGx] * -0.5745488 + zeroCrossingRate[indexGy] * -7.3140146 + zeroCrossingRate[indexRoll] * -3.3783573 + rootMeanSquare[indexAx] * 1.2890156 + rootMeanSquare[indexAz] * -4.2910176 + rootMeanSquare[indexTotalGyro] * 0.005179  + rootMeanSquare[indexRoll] * 0.0073107 + rootMeanSquare[indexPitch] * -0.0655836 + skewness[indexAx] * -0.0628997 + skewness[indexAy] * -0.4898532 + skewness[indexAz] * -0.0278635 + skewness[indexGx] * -0.0309364 + skewness[indexTotalAcc] * -0.1075365 + skewness[indexTotalGyro] * 0.9693804 + skewness[indexPitch] * 0.1495399 + kurtosis[indexAy] * 0.0688011 + kurtosis[indexAz] * 0.0778666 + kurtosis[indexGx] * 0.0414931 + kurtosis[indexGz] * -0.0674885 + kurtosis[indexTotalAcc] * 0.0727781 + kurtosis[indexTotalGyro] * 0.2575505 + kurtosis[indexRoll] * 0.0265135 + meanAbsDev[indexAx] * 1.7759647 + meanAbsDev[indexGx] * -0.0262091 + meanAbsDev[indexGy] * 0.00219   + meanAbsDev[indexGz] * 0.015224  + meanAbsDev[indexRoll] * 0.0658968 + fftMaxFreq[indexAz] * 1.2884386 + fftMaxFreq[indexGz] * 0.0958673 + fftMaxFreq[indexPitch] * -0.0470748 + fftEnergy[indexAy] * 0.3622794 + fftEnergy[indexGx] * 0.0000636 + fftEnergy[indexGy] * -0.0000571 + fftEnergy[indexGz] * 0.0000058 + fftEnergy[indexRoll] * 0.0000421 + fftEntropy[indexAx] * -0.0191174 + fftEntropy[indexGz] * 0.0000213 + fftEntropy[indexTotalAcc] * 0.0110862 + fftEntropy[indexTotalGyro] * -0.0000395 + fftEntropy[indexRoll] * -0.000005 + covarianceAxAy * -10.3307659 + covarianceAyAz * -1.0759107 + covarianceGxGz * -0.0000126 + covarianceGyGz * 0.0001368 + correlationGxGz * -0.3028053;
        */
        //MODEL HYBRID without JimJim
        /*
        Class[0] = -15.5310236 +  standardDeviation[indexTotalAcc] * -5.9386719 + standardDeviation[indexTotalGyro] * -0.0015387 + mean[indexAx] * -0.8271061 + mean[indexAy] * -4.7355358 + mean[indexGx] * 0.0063534 + mean[indexGy] * 0.0832427 + mean[indexGz] * -0.0466022 + mean[indexTotalAcc] * 18.910222 + mean[indexTotalGyro] * 0.0605894 + mean[indexRoll] * -0.01433 + mean[indexPitch] * -0.002557 + variance[indexAx] * 25.5913934 + variance[indexAz] * 4.7261697 + variance[indexGz] * -0.0006328 + variance[indexTotalAcc] * -22.8753054 + variance[indexTotalGyro] * -0.0000233 + variance[indexRoll] * -0.0000574 + variance[indexPitch] * -0.0001127 + zeroCrossingRate[indexAz] * -15.6952997 + zeroCrossingRate[indexGx] * 4.3889244 + zeroCrossingRate[indexGy] * -3.3902775 + zeroCrossingRate[indexGz] * 8.2011043 + zeroCrossingRate[indexPitch] * 5.5040312 + rootMeanSquare[indexAx] * 0.861674  + rootMeanSquare[indexAy] * -2.9559098 + rootMeanSquare[indexAz] * -4.5289774 + rootMeanSquare[indexGz] * -0.0317129 + rootMeanSquare[indexRoll] * 0.0048024 + rootMeanSquare[indexPitch] * 0.0873576 + skewness[indexAx] * 0.3991966 + skewness[indexAy] * -0.8726485 + skewness[indexAz] * 0.746787  + skewness[indexGy] * -0.1254825 + skewness[indexGz] * 0.0271704 + skewness[indexTotalAcc] * 0.6932068 + skewness[indexTotalGyro] * -0.8603945 + skewness[indexRoll] * 0.0933045 + skewness[indexPitch] * -0.0729999 + kurtosis[indexAx] * 0.0848105 + kurtosis[indexAz] * 0.0465895 + kurtosis[indexGx] * 0.094173  + kurtosis[indexGy] * -0.0180579 + kurtosis[indexTotalAcc] * -0.0128363 + kurtosis[indexTotalGyro] * 0.487019  + kurtosis[indexRoll] * 0.0303563 + meanAbsDev[indexAx] * -22.5772484 + meanAbsDev[indexAz] * 4.3582275 + meanAbsDev[indexGy] * -0.0912165 + meanAbsDev[indexGz] * -0.0547337 + meanAbsDev[indexPitch] * 0.0054323 + fftMaxFreq[indexAz] * -0.4052093 + fftMaxFreq[indexGx] * -0.2064425 + fftMaxFreq[indexGy] * 0.1298333 + fftMaxFreq[indexGz] * -0.3032971 + fftMaxFreq[indexRoll] * 0.2972156 + fftMaxFreq[indexPitch] * 0.3581727 + fftEnergy[indexAx] * -9.151416 + fftEnergy[indexAy] * 2.451568  + fftEnergy[indexGy] * -0.0005226 + fftEnergy[indexGz] * -0.0001312 + fftEntropy[indexAy] * 0.0464696 + fftEntropy[indexAz] * 0.0508866 + fftEntropy[indexGx] * -0.0000149 + fftEntropy[indexGz] * 0.0000645 + fftEntropy[indexTotalAcc] * -0.0665335 + fftEntropy[indexPitch] * 0.0000045 + covarianceAxAy * 9.1705935 + covarianceGxGy * -0.0004342 + covarianceGxGz * -0.0005356 + covarianceGyGz * 0.0012154 + correlationAxAy * -0.1036659 + correlationAyAz * 1.070605  + correlationGxGy * 0.2213151 + correlationGxGz * 0.4441181 + correlationGyGz * -1.3900119 ;
        Class[1] = 19.3011682 +  standardDeviation[indexAx] * -7.6423739 + standardDeviation[indexAy] * 1.9213707 + standardDeviation[indexGx] * 0.0992174 + standardDeviation[indexTotalAcc] * 8.0193171 + standardDeviation[indexRoll] * -0.0424969 + mean[indexAx] * 7.7217534 + mean[indexAy] * -1.2893486 + mean[indexGx] * -0.0025669 + mean[indexGy] * -0.0586886 + mean[indexGz] * -0.0225798 + mean[indexTotalAcc] * -18.1309966 + mean[indexTotalGyro] * 0.0037121 + mean[indexPitch] * 0.0683476 + variance[indexAz] * -28.5441804 + variance[indexGy] * 0.0006262 + variance[indexGz] * -0.0001663 + variance[indexTotalGyro] * -0.0007342 + variance[indexRoll] * 0.0022679 + zeroCrossingRate[indexAx] * 5.6745648 + zeroCrossingRate[indexAz] * 49.1454461 + zeroCrossingRate[indexGx] * -10.7312553 + zeroCrossingRate[indexGy] * -16.0725569 + zeroCrossingRate[indexGz] * 14.6626877 + zeroCrossingRate[indexPitch] * -10.6246162 + rootMeanSquare[indexAx] * 0.434131  + rootMeanSquare[indexAz] * -4.4968498 + rootMeanSquare[indexGy] * -0.0217317 + rootMeanSquare[indexGz] * -0.2354569 + skewness[indexAx] * -0.030945 + skewness[indexAy] * 1.3002267 + skewness[indexAz] * 0.4820172 + skewness[indexGx] * -0.4778624 + skewness[indexGy] * 0.0585563 + skewness[indexGz] * -0.0953163 + skewness[indexTotalAcc] * -1.3748507 + skewness[indexTotalGyro] * 2.6174706 + skewness[indexRoll] * 0.065261  + kurtosis[indexAx] * 0.0014131 + kurtosis[indexAz] * -0.0590076 + kurtosis[indexGx] * -0.637529 + kurtosis[indexGy] * 0.1494073 + kurtosis[indexGz] * 0.0943836 + kurtosis[indexTotalAcc] * -0.2669495 + kurtosis[indexTotalGyro] * -0.9809367 + kurtosis[indexRoll] * 0.0031826 + kurtosis[indexPitch] * -0.509382 + meanAbsDev[indexAy] * 12.0823821 + meanAbsDev[indexGz] * -0.0293241 + meanAbsDev[indexTotalAcc] * 2.680308  + meanAbsDev[indexRoll] * -0.2562187 + meanAbsDev[indexPitch] * 0.0183939 + fftMaxFreq[indexAx] * -0.304093 + fftMaxFreq[indexAy] * 1.0898642 + fftMaxFreq[indexAz] * -13.8400981 + fftMaxFreq[indexGx] * 0.7793031 + fftMaxFreq[indexGy] * -0.0415012 + fftMaxFreq[indexGz] * 0.2745771 + fftMaxFreq[indexRoll] * 0.4592187 + fftMaxFreq[indexPitch] * -1.0297779 + fftEnergy[indexAx] * -4.4314774 + fftEnergy[indexAy] * -2.4473572 + fftEnergy[indexAz] * 2.9609654 + fftEnergy[indexGx] * 0.0000302 + fftEnergy[indexGz] * -0.0000769 + fftEnergy[indexPitch] * -0.0000939 + fftEntropy[indexAy] * 0.0196172 + fftEntropy[indexAz] * -0.0190797 + fftEntropy[indexGx] * 0.0000185 + fftEntropy[indexGy] * -0.0000421 + fftEntropy[indexRoll] * -0.0000827 + fftEntropy[indexPitch] * -0.0000388 + covarianceAxAy * 12.9729288 + covarianceAyAz * -5.9965885 + covarianceGxGy * 0.0010079 + covarianceGxGz * -0.0015479 + covarianceGyGz * 0.0010271 + correlationAxAy * 0.1369844 + correlationAyAz * -2.0330638 + correlationGxGy * -2.3150656 + correlationGxGz * -1.6095762 + correlationGyGz * -0.9205449 ;
        Class[2] = -15.4528367 +  standardDeviation[indexAx] * -2.5174935 + standardDeviation[indexAy] * 0.3189526 + standardDeviation[indexGy] * 0.0901936 + standardDeviation[indexGz] * 0.0463822 + standardDeviation[indexTotalAcc] * -7.5519288 + mean[indexAx] * 0.2753623 + mean[indexAy] * 0.3037832 + mean[indexGx] * -0.044885 + mean[indexGy] * -0.101026 + mean[indexGz] * -0.0044097 + mean[indexPitch] * 0.055934  + variance[indexAx] * -11.3285898 + variance[indexAy] * 5.1358173 + variance[indexAz] * 36.7414087 + variance[indexGx] * 0.0000125 + variance[indexGz] * 0.0001262 + variance[indexTotalAcc] * 8.2701405 + variance[indexTotalGyro] * 0.0000612 + variance[indexRoll] * -0.000118 + zeroCrossingRate[indexAx] * 4.1779907 + zeroCrossingRate[indexAy] * 1.5555724 + zeroCrossingRate[indexAz] * 75.6114987 + zeroCrossingRate[indexGx] * 22.3280854 + zeroCrossingRate[indexGy] * 8.3397022 + rootMeanSquare[indexAy] * -1.3270171 + rootMeanSquare[indexAz] * 0.2613265 + rootMeanSquare[indexGz] * 0.0382933 + rootMeanSquare[indexTotalAcc] * 8.9460967 + rootMeanSquare[indexTotalGyro] * 0.0030695 + rootMeanSquare[indexPitch] * -0.0023419 + skewness[indexAx] * -1.4052953 + skewness[indexAy] * -0.6953035 + skewness[indexGx] * 0.030742  + skewness[indexGy] * 0.1918498 + skewness[indexGz] * 0.1961354 + skewness[indexTotalAcc] * -0.1030613 + skewness[indexPitch] * -0.2803241 + kurtosis[indexAx] * 0.1717077 + kurtosis[indexAy] * 0.261447  + kurtosis[indexAz] * -0.1512359 + kurtosis[indexGx] * 0.1841554 + kurtosis[indexGy] * 0.0315768 + kurtosis[indexGz] * -0.2530665 + kurtosis[indexTotalAcc] * 0.0489828 + kurtosis[indexTotalGyro] * 0.1280864 + kurtosis[indexRoll] * -0.4400006 + meanAbsDev[indexAx] * 9.9494923 + meanAbsDev[indexAz] * -51.6622506 + meanAbsDev[indexTotalAcc] * -50.658833 + meanAbsDev[indexRoll] * 0.054835  + fftMaxFreq[indexAx] * -0.6058082 + fftMaxFreq[indexAy] * 0.4057488 + fftMaxFreq[indexAz] * -26.0262235 + fftMaxFreq[indexRoll] * 0.1166503 + fftMaxFreq[indexPitch] * -0.8871423 + fftEnergy[indexAy] * -5.1242577 + fftEnergy[indexGx] * 0.0000234 + fftEnergy[indexGy] * -0.0006595 + fftEnergy[indexGz] * -0.0001046 + fftEnergy[indexTotalAcc] * 0.986733  + fftEntropy[indexAx] * 0.0054551 + fftEntropy[indexAz] * -0.0955972 + fftEntropy[indexGy] * -0.0001131 + fftEntropy[indexGz] * -0.000009 + fftEntropy[indexRoll] * 0.0001373 + fftEntropy[indexPitch] * -0.0002126 + covarianceAxAy * -31.4950761 + covarianceAyAz * 8.1678018 + covarianceGxGy * -0.0002747 + covarianceGxGz * 0.0000384 + covarianceGyGz * -0.0001273 + correlationAxAy * 0.8149447 + correlationGxGy * -0.2701441 + correlationGxGz * 0.5987689 + correlationGyGz * 1.6718369 ;
        Class[3] = -42.7322441 +  standardDeviation[indexAy] * -2.6441985 + standardDeviation[indexAz] * 16.8473936 + standardDeviation[indexGy] * 0.0572525 + standardDeviation[indexGz] * 0.1001674 + standardDeviation[indexTotalAcc] * -11.1959373 + mean[indexAx] * 1.0068282 + mean[indexAy] * 1.2043426 + mean[indexAz] * 2.6628803 + mean[indexGx] * -0.0396626 + mean[indexTotalAcc] * 41.3341398 + variance[indexAx] * -10.7098572 + variance[indexAy] * -22.6894411 + variance[indexAz] * -4.9240494 + variance[indexGx] * -0.0000908 + variance[indexGz] * -0.000364 + variance[indexTotalAcc] * -2.2669311 + variance[indexTotalGyro] * -0.0001127 + variance[indexPitch] * 0.0002101 + zeroCrossingRate[indexAx] * 6.615289  + zeroCrossingRate[indexAz] * -7.2911509 + zeroCrossingRate[indexGx] * 9.6291177 + zeroCrossingRate[indexGy] * 6.5552954 + zeroCrossingRate[indexGz] * -6.5114774 + rootMeanSquare[indexAx] * 2.4499433 + rootMeanSquare[indexAy] * 1.7645299 + rootMeanSquare[indexPitch] * 0.0238588 + skewness[indexAx] * 2.5294664 + skewness[indexAy] * 1.1566886 + skewness[indexAz] * -0.9610273 + skewness[indexGx] * -0.1473359 + skewness[indexGy] * 0.0530578 + skewness[indexGz] * -1.137607 + skewness[indexTotalAcc] * -0.3604027 + skewness[indexTotalGyro] * 0.7364951 + skewness[indexRoll] * -0.675059 + kurtosis[indexAx] * -0.3039037 + kurtosis[indexAy] * -0.0556997 + kurtosis[indexGx] * -0.0400384 + kurtosis[indexGy] * 0.1998885 + kurtosis[indexGz] * -1.2648536 + kurtosis[indexTotalAcc] * 0.1491993 + kurtosis[indexTotalGyro] * -1.07376 + kurtosis[indexRoll] * -0.7790198 + kurtosis[indexPitch] * -0.2742233 + meanAbsDev[indexAx] * 3.8078056 + meanAbsDev[indexAy] * -3.6420931 + meanAbsDev[indexGz] * 0.0711595 + meanAbsDev[indexTotalAcc] * -35.2179221 + meanAbsDev[indexRoll] * -0.0321483 + fftMaxFreq[indexAy] * 0.5300414 + fftMaxFreq[indexAz] * -11.734232 + fftMaxFreq[indexGx] * 0.2553843 + fftMaxFreq[indexGy] * 0.1417552 + fftMaxFreq[indexGz] * -0.3389335 + fftMaxFreq[indexRoll] * 0.2272192 + fftMaxFreq[indexPitch] * 0.8301254 + fftEnergy[indexAy] * -8.9122003 + fftEnergy[indexGx] * -0.0000537 + fftEnergy[indexGy] * -0.000411 + fftEnergy[indexTotalGyro] * -0.0000857 + fftEnergy[indexRoll] * -0.0003138 + fftEnergy[indexPitch] * -0.0000913 + fftEntropy[indexAx] * 0.0142875 + fftEntropy[indexAy] * -0.095712 + fftEntropy[indexAz] * 0.0585171 + fftEntropy[indexGz] * -0.0000263 + fftEntropy[indexTotalAcc] * -0.0913694 + fftEntropy[indexTotalGyro] * -0.0000047 + covarianceAxAy * -17.0568711 + covarianceAyAz * -3.4868925 + covarianceGxGy * -0.0001005 + covarianceGxGz * 0.0000673 + covarianceGyGz * 0.0000939 + correlationAyAz * -1.0182386 + correlationGxGy * 0.3993112 + correlationGxGz * -1.207022 + correlationGyGz * -1.0598076 ;
        Class[4] = 1.2184326 +  standardDeviation[indexAx] * 3.2933748 + standardDeviation[indexAy] * 3.8469809 + standardDeviation[indexGx] * 0.0872566 + standardDeviation[indexGy] * 0.0220064 + standardDeviation[indexGz] * 0.0205278 + standardDeviation[indexTotalAcc] * 1.1068067 + standardDeviation[indexRoll] * 0.0340783 + mean[indexAy] * -0.2762366 + mean[indexAz] * 8.3207423 + mean[indexGx] * -0.004344 + mean[indexGy] * -0.1267118 + mean[indexGz] * -0.0401933 + mean[indexTotalAcc] * -13.105103 + mean[indexRoll] * -0.0740436 + mean[indexPitch] * 0.0159127 + variance[indexGy] * -0.0001224 + variance[indexGz] * -0.0006281 + variance[indexTotalGyro] * -0.0003673 + zeroCrossingRate[indexAx] * 0.973195  + zeroCrossingRate[indexAy] * 7.4417085 + zeroCrossingRate[indexAz] * -61.2606128 + zeroCrossingRate[indexGy] * -5.9612179 + zeroCrossingRate[indexGz] * -9.8133593 + zeroCrossingRate[indexPitch] * -1.3012928 + rootMeanSquare[indexAx] * -2.4317602 + rootMeanSquare[indexAz] * 3.2948563 + rootMeanSquare[indexGy] * -0.0068913 + skewness[indexAx] * 0.3894608 + skewness[indexAy] * 2.3747076 + skewness[indexGx] * -0.7735089 + skewness[indexGy] * -0.3825122 + skewness[indexGz] * 0.3438246 + skewness[indexTotalAcc] * 0.3962435 + skewness[indexRoll] * 0.1411733 + skewness[indexPitch] * 0.7690913 + kurtosis[indexAy] * -0.8276457 + kurtosis[indexAz] * -0.7137373 + kurtosis[indexGx] * -0.5014539 + kurtosis[indexGy] * -0.1249393 + kurtosis[indexGz] * 0.0854211 + kurtosis[indexTotalAcc] * -0.228831 + kurtosis[indexTotalGyro] * 0.5584428 + kurtosis[indexRoll] * -0.0274977 + meanAbsDev[indexAx] * 3.4186104 + meanAbsDev[indexAy] * 8.1825998 + meanAbsDev[indexGx] * -0.0340753 + meanAbsDev[indexTotalAcc] * 2.1525622 + meanAbsDev[indexTotalGyro] * -0.0815531 + meanAbsDev[indexPitch] * -0.0240476 + fftMaxFreq[indexAx] * 0.2665949 + fftMaxFreq[indexAy] * 1.6375904 + fftMaxFreq[indexGx] * 0.954382  + fftMaxFreq[indexGy] * -0.1710198 + fftMaxFreq[indexGz] * -0.38757 + fftMaxFreq[indexRoll] * -0.0972495 + fftMaxFreq[indexPitch] * -0.5224139 + fftEnergy[indexAx] * 3.209384  + fftEnergy[indexAy] * -1.0045266 + fftEnergy[indexGx] * -0.0000108 + fftEnergy[indexGy] * 0.0000532 + fftEnergy[indexGz] * -0.0001218 + fftEnergy[indexTotalGyro] * -0.000005 + fftEnergy[indexRoll] * 0.0003022 + fftEnergy[indexPitch] * -0.000346 + fftEntropy[indexAx] * -0.0333945 + fftEntropy[indexAy] * -0.0106379 + fftEntropy[indexAz] * 0.0188566 + fftEntropy[indexGx] * -0.0000095 + fftEntropy[indexGy] * -0.000023 + fftEntropy[indexGz] * -0.0000258 + fftEntropy[indexTotalAcc] * -0.0028412 + fftEntropy[indexTotalGyro] * -0.0000232 + covarianceAxAy * 22.6305637 + covarianceAyAz * -5.282744 + covarianceGxGy * 0.0001873 + covarianceGxGz * 0.0000641 + covarianceGyGz * -0.0008887 + correlationAxAy * -0.4868707 + correlationAyAz * -1.6076394 + correlationGxGy * -0.2046069 + correlationGxGz * 0.2920176 + correlationGyGz * 1.8854842 ;
        Class[5] = 6.8091784 +  standardDeviation[indexGx] * 0.009572  + standardDeviation[indexGz] * 0.0090353 + mean[indexAy] * -5.0976704 + mean[indexAz] * -4.9908204 + mean[indexGx] * 0.0193882 + mean[indexGy] * 0.0730326 + mean[indexGz] * 0.0028691 + mean[indexRoll] * -0.0324995 + variance[indexAy] * -9.7255775 + variance[indexGy] * 0.0009614 + variance[indexGz] * -0.0000224 + variance[indexTotalAcc] * 17.210339 + variance[indexTotalGyro] * -0.0007059 + variance[indexRoll] * -0.0008979 + variance[indexPitch] * -0.0001332 + zeroCrossingRate[indexAx] * 4.8433561 + zeroCrossingRate[indexAz] * 40.6755654 + zeroCrossingRate[indexGx] * -10.8304145 + zeroCrossingRate[indexGy] * -1.7298465 + zeroCrossingRate[indexGz] * -10.5359713 + zeroCrossingRate[indexPitch] * -11.3359525 + rootMeanSquare[indexAx] * -0.1913045 + rootMeanSquare[indexAy] * 0.5549026 + rootMeanSquare[indexGy] * -0.0962463 + rootMeanSquare[indexRoll] * -0.0736351 + rootMeanSquare[indexPitch] * 0.0208294 + skewness[indexAx] * -0.4769358 + skewness[indexAy] * 1.0663967 + skewness[indexAz] * 0.2030137 + skewness[indexGx] * 0.5656046 + skewness[indexGy] * 0.5690464 + skewness[indexGz] * -0.1194456 + skewness[indexTotalAcc] * 0.1101106 + skewness[indexTotalGyro] * -0.1893006 + skewness[indexRoll] * 0.3408674 + skewness[indexPitch] * 1.0675906 + kurtosis[indexAx] * 0.0192911 + kurtosis[indexAy] * -0.7733782 + kurtosis[indexAz] * -0.5515075 + kurtosis[indexGx] * 0.1219741 + kurtosis[indexGy] * -0.0520129 + kurtosis[indexGz] * 0.1944352 + kurtosis[indexTotalAcc] * -0.0211102 + kurtosis[indexTotalGyro] * -0.056061 + kurtosis[indexRoll] * -0.0541893 + kurtosis[indexPitch] * 0.0142754 + meanAbsDev[indexAx] * 4.0245686 + meanAbsDev[indexAy] * 9.6549527 + meanAbsDev[indexGz] * -0.0027702 + fftMaxFreq[indexAx] * -0.052509 + fftMaxFreq[indexAy] * 1.2952432 + fftMaxFreq[indexGx] * 0.5681549 + fftMaxFreq[indexGy] * 0.1311308 + fftMaxFreq[indexGz] * -0.1127378 + fftMaxFreq[indexRoll] * 0.3675779 + fftEnergy[indexAx] * -0.7882389 + fftEnergy[indexAy] * 0.2205587 + fftEnergy[indexGx] * -0.0000037 + fftEnergy[indexGy] * -0.0001112 + fftEnergy[indexGz] * -0.0000457 + fftEnergy[indexTotalGyro] * -0.000013 + fftEnergy[indexRoll] * -0.0004784 + fftEntropy[indexAx] * 0.0157747 + fftEntropy[indexAy] * 0.0167775 + fftEntropy[indexGz] * -0.0000303 + fftEntropy[indexTotalAcc] * -0.0126979 + fftEntropy[indexRoll] * -0.0000323 + fftEntropy[indexPitch] * -0.0001439 + covarianceAyAz * -0.8735229 + covarianceGxGy * 0.0000686 + covarianceGyGz * 0.0002625 + correlationAxAy * 0.2432102 + correlationAyAz * -2.4716458 + correlationGxGy * -0.9487834 + correlationGyGz * 0.3986131 ;
        Class[6] = -0.0102753 +  standardDeviation[indexGx] * -0.0017961 + standardDeviation[indexRoll] * 0.0127796 + standardDeviation[indexPitch] * 0.0995421 + mean[indexAx] * -3.7520873 + mean[indexAz] * 0.5925326 + mean[indexGx] * 0.0189385 + mean[indexGy] * 0.0238457 + mean[indexGz] * 0.0245099 + mean[indexRoll] * 0.0396359 + mean[indexPitch] * 0.0084539 + variance[indexAx] * 21.1574198 + variance[indexAy] * -3.0220985 + variance[indexGx] * -0.0001013 + variance[indexGy] * -0.0000252 + variance[indexGz] * 0.0000878 + variance[indexTotalAcc] * 0.6153206 + variance[indexTotalGyro] * 0.0001832 + variance[indexPitch] * -0.001537 + zeroCrossingRate[indexAy] * -3.6521106 + zeroCrossingRate[indexAz] * -10.4230163 + zeroCrossingRate[indexGx] * -16.8182581 + zeroCrossingRate[indexGz] * -2.1848306 + rootMeanSquare[indexAx] * 0.9480119 + rootMeanSquare[indexAy] * -1.366378 + rootMeanSquare[indexAz] * 2.9653193 + rootMeanSquare[indexGx] * -0.0183594 + rootMeanSquare[indexGy] * -0.0018931 + skewness[indexAx] * 0.503358  + skewness[indexAy] * 1.438111  + skewness[indexAz] * 0.690791  + skewness[indexGx] * 0.6159829 + skewness[indexGy] * 0.2218369 + skewness[indexTotalGyro] * 0.5586279 + kurtosis[indexAx] * 0.1417656 + kurtosis[indexAy] * -1.0343201 + kurtosis[indexGx] * -1.5372513 + kurtosis[indexGy] * 0.0385781 + kurtosis[indexGz] * 0.1802857 + kurtosis[indexTotalAcc] * -0.5812621 + kurtosis[indexTotalGyro] * 0.1407532 + kurtosis[indexRoll] * -0.1350076 + kurtosis[indexPitch] * 0.0240873 + meanAbsDev[indexAx] * -13.394893 + meanAbsDev[indexAy] * -1.5224623 + meanAbsDev[indexGx] * 0.0609684 + meanAbsDev[indexGz] * -0.0544794 + meanAbsDev[indexPitch] * 0.1352642 + fftMaxFreq[indexAz] * -2.8601812 + fftMaxFreq[indexGx] * 0.7314621 + fftMaxFreq[indexGy] * -0.0397315 + fftMaxFreq[indexRoll] * -0.1909392 + fftEnergy[indexAy] * 0.4076508 + fftEnergy[indexGx] * 0.0000345 + fftEnergy[indexGy] * -0.0000115 + fftEnergy[indexGz] * 0.0000095 + fftEnergy[indexPitch] * -0.0000684 + fftEntropy[indexAx] * 0.0047798 + fftEntropy[indexAy] * -0.0065868 + fftEntropy[indexAz] * 0.0045738 + fftEntropy[indexGy] * 0.0000347 + fftEntropy[indexTotalGyro] * 0.0001076 + covarianceAxAy * -4.1159404 + covarianceAyAz * -7.6011299 + covarianceGxGy * 0.0003718 + covarianceGxGz * -0.0004052 + covarianceGyGz * -0.000781 + correlationAxAy * -0.9050126 + correlationAyAz * 6.2145453 + correlationGxGz * -0.5755056 + correlationGyGz * 2.2284933 ;
        Class[7] = -3.9923035 +  standardDeviation[indexGy] * 0.0973505 + standardDeviation[indexTotalAcc] * 12.8472263 + standardDeviation[indexRoll] * 0.0150154 + mean[indexAy] * 1.2381684 + mean[indexAz] * 0.7148494 + mean[indexGx] * -0.0130743 + mean[indexGy] * -0.140014 + mean[indexGz] * 0.0022523 + mean[indexTotalAcc] * -2.1000605 + mean[indexTotalGyro] * -0.0205356 + mean[indexRoll] * 0.0297606 + mean[indexPitch] * 0.0040258 + variance[indexAx] * -5.7401935 + variance[indexAy] * -19.2103571 + variance[indexAz] * -6.0104331 + variance[indexGx] * -0.0000991 + variance[indexGz] * -0.0000307 + variance[indexRoll] * 0.0000301 + variance[indexPitch] * 0.0000845 + zeroCrossingRate[indexGx] * 0.7048542 + zeroCrossingRate[indexGy] * 6.6712972 + zeroCrossingRate[indexPitch] * -2.0508217 + rootMeanSquare[indexGy] * -0.1640111 + rootMeanSquare[indexPitch] * 0.0540554 + skewness[indexAx] * -0.2050498 + skewness[indexAz] * -0.0902819 + skewness[indexGx] * -0.2396803 + skewness[indexGy] * -0.0968108 + skewness[indexGz] * 0.6769968 + skewness[indexTotalAcc] * -0.3201316 + skewness[indexTotalGyro] * 0.5911328 + skewness[indexRoll] * 0.3668913 + skewness[indexPitch] * -0.1940022 + kurtosis[indexAx] * -0.1405182 + kurtosis[indexAy] * -0.2356053 + kurtosis[indexAz] * -0.0946791 + kurtosis[indexGx] * -0.9243538 + kurtosis[indexGy] * 0.1103197 + kurtosis[indexTotalAcc] * 0.0720936 + kurtosis[indexTotalGyro] * -0.4919564 + kurtosis[indexRoll] * 0.0390285 + kurtosis[indexPitch] * -0.057662 + meanAbsDev[indexAx] * -2.6799952 + meanAbsDev[indexAy] * 4.1715469 + meanAbsDev[indexGx] * 0.0384148 + meanAbsDev[indexTotalAcc] * 10.8046315 + meanAbsDev[indexTotalGyro] * 0.0127786 + meanAbsDev[indexRoll] * -0.0342388 + meanAbsDev[indexPitch] * 0.0847741 + fftMaxFreq[indexAx] * -0.3477431 + fftMaxFreq[indexAz] * 4.5047655 + fftMaxFreq[indexGx] * 0.9542643 + fftMaxFreq[indexGy] * 0.0616162 + fftMaxFreq[indexGz] * -0.2792846 + fftMaxFreq[indexRoll] * 0.1600219 + fftMaxFreq[indexPitch] * 0.5617402 + fftEnergy[indexAx] * -6.7854228 + fftEnergy[indexAy] * 1.5315051 + fftEnergy[indexAz] * 0.4080444 + fftEnergy[indexGy] * -0.0002464 + fftEnergy[indexRoll] * -0.0000693 + fftEntropy[indexAx] * -0.089042 + fftEntropy[indexAy] * -0.031751 + fftEntropy[indexGx] * 0.0000173 + fftEntropy[indexTotalGyro] * 0.0000308 + fftEntropy[indexRoll] * 0.0000351 + covarianceAxAy * -14.4955011 + covarianceAyAz * 8.5330345 + covarianceGxGy * -0.0003575 + covarianceGxGz * 0.0001259 + covarianceGyGz * -0.0004444 + correlationAxAy * 2.06107   + correlationAyAz * 3.3911875 + correlationGxGy * 0.2835206 + correlationGxGz * 0.8096412 + correlationGyGz * -0.652802 ;
        Class[8] = -0.0819644 +  standardDeviation[indexAx] * 0.8867814 + standardDeviation[indexAz] * -1.4496416 + standardDeviation[indexGx] * 0.0009816 + standardDeviation[indexGy] * -0.0648293 + standardDeviation[indexTotalGyro] * 0.0084914 + mean[indexAx] * 0.2657316 + mean[indexGx] * 0.0195767 + mean[indexGy] * 0.0126364 + mean[indexGz] * 0.0072877 + mean[indexTotalAcc] * 0.7830923 + mean[indexRoll] * -0.0086623 + mean[indexPitch] * -0.0486569 + variance[indexAy] * 15.5514344 + variance[indexAz] * -1.189991 + variance[indexGx] * -0.0000191 + variance[indexGz] * -0.0001406 + variance[indexTotalAcc] * -2.2874086 + variance[indexRoll] * 0.0001093 + variance[indexPitch] * 0.0002251 + zeroCrossingRate[indexAx] * 2.3173267 + zeroCrossingRate[indexAy] * -8.3391942 + zeroCrossingRate[indexAz] * 5.2002294 + zeroCrossingRate[indexGy] * 6.3150071 + zeroCrossingRate[indexGz] * -1.0296662 + rootMeanSquare[indexAx] * -1.2453511 + rootMeanSquare[indexGy] * 0.0721241 + rootMeanSquare[indexGz] * 0.0319899 + rootMeanSquare[indexPitch] * -0.0013915 + skewness[indexAx] * -0.0342212 + skewness[indexAz] * 1.1925206 + skewness[indexGx] * 0.2042586 + skewness[indexGy] * 1.5680763 + skewness[indexGz] * -0.2683408 + skewness[indexTotalAcc] * -0.70161 + skewness[indexTotalGyro] * -1.9842624 + skewness[indexRoll] * -0.1846357 + skewness[indexPitch] * 0.1846881 + kurtosis[indexAx] * 0.0837997 + kurtosis[indexAy] * 0.0797087 + kurtosis[indexAz] * -0.2575759 + kurtosis[indexGx] * -0.1019186 + kurtosis[indexGy] * -0.2380151 + kurtosis[indexGz] * 0.2650857 + kurtosis[indexTotalAcc] * -0.4579192 + kurtosis[indexTotalGyro] * 0.0576048 + kurtosis[indexRoll] * 0.0193465 + kurtosis[indexPitch] * -0.0293336 + meanAbsDev[indexAx] * 4.6117484 + meanAbsDev[indexAy] * -25.8787298 + meanAbsDev[indexGx] * 0.0012357 + meanAbsDev[indexTotalGyro] * 0.0193148 + meanAbsDev[indexPitch] * -0.0070842 + fftMaxFreq[indexAy] * 0.8232885 + fftMaxFreq[indexGx] * -0.6446729 + fftMaxFreq[indexGy] * 0.1307883 + fftMaxFreq[indexGz] * 0.553167  + fftMaxFreq[indexRoll] * 0.3454332 + fftMaxFreq[indexPitch] * 0.348434  + fftEnergy[indexAx] * -1.579139 + fftEnergy[indexAy] * 0.9974896 + fftEnergy[indexAz] * -0.5017994 + fftEnergy[indexGx] * -0.0000121 + fftEnergy[indexGy] * 0.0001007 + fftEntropy[indexAx] * 0.0600028 + fftEntropy[indexGy] * -0.0000382 + fftEntropy[indexRoll] * 0.0000234 + fftEntropy[indexPitch] * -0.0000085 + covarianceAxAy * 22.7769782 + covarianceGxGy * 0.0001288 + covarianceGxGz * 0.0000074 + covarianceGyGz * 0.0001072 + correlationAxAy * -1.4248191 + correlationAyAz * 0.7362042 + correlationGxGy * -0.5826874 + correlationGxGz * -0.5210131 + correlationGyGz * -0.215342 ;
        Class[9] = 2.7663498 +  standardDeviation[indexAy] * 0.6761934 + standardDeviation[indexGz] * 0.0090332 + standardDeviation[indexTotalGyro] * 0.0027286 + mean[indexAx] * -4.9281464 + mean[indexGx] * -0.0047525 + mean[indexGy] * 0.0052994 + mean[indexGz] * 0.0070569 + mean[indexRoll] * -0.0124817 + mean[indexPitch] * 0.0086276 + variance[indexAx] * 7.1897859 + variance[indexTotalAcc] * -4.01196 + variance[indexTotalGyro] * 0.0001333 + variance[indexRoll] * 0.000189  + variance[indexPitch] * 0.0011878 + zeroCrossingRate[indexAx] * 8.0649976 + zeroCrossingRate[indexAz] * -12.0739251 + zeroCrossingRate[indexGx] * -1.9048288 + zeroCrossingRate[indexGy] * -5.2477562 + zeroCrossingRate[indexGz] * 12.5087357 + zeroCrossingRate[indexPitch] * -4.1023028 + rootMeanSquare[indexAx] * -1.0116616 + rootMeanSquare[indexAz] * -0.2881306 + rootMeanSquare[indexGx] * 0.0096308 + rootMeanSquare[indexGy] * 0.0797999 + rootMeanSquare[indexGz] * 0.0151562 + rootMeanSquare[indexTotalAcc] * -3.7521197 + rootMeanSquare[indexRoll] * -0.0587077 + skewness[indexAx] * -1.2168444 + skewness[indexAy] * -0.0425372 + skewness[indexAz] * -0.3492678 + skewness[indexGx] * -1.3635665 + skewness[indexGy] * -3.4703111 + skewness[indexTotalAcc] * 0.4443212 + skewness[indexTotalGyro] * -2.42364 + skewness[indexRoll] * -0.6344482 + skewness[indexPitch] * -0.2617925 + kurtosis[indexAx] * 0.0588491 + kurtosis[indexAy] * 0.4087701 + kurtosis[indexAz] * -0.1582051 + kurtosis[indexGx] * -0.135094 + kurtosis[indexGy] * -0.7814983 + kurtosis[indexGz] * 0.2450073 + kurtosis[indexTotalAcc] * -0.0863779 + kurtosis[indexTotalGyro] * 0.1774719 + kurtosis[indexRoll] * -0.0799034 + meanAbsDev[indexAx] * -3.9382964 + meanAbsDev[indexAy] * -0.2238207 + meanAbsDev[indexGy] * -0.079845 + meanAbsDev[indexGz] * -0.0014189 + meanAbsDev[indexTotalGyro] * 0.0127933 + meanAbsDev[indexRoll] * -0.0186106 + meanAbsDev[indexPitch] * -0.1682325 + fftMaxFreq[indexAy] * -4.0300365 + fftMaxFreq[indexGx] * -0.1382008 + fftMaxFreq[indexGy] * -1.0710692 + fftMaxFreq[indexGz] * 0.2405248 + fftMaxFreq[indexRoll] * 0.2911612 + fftMaxFreq[indexPitch] * -0.4087521 + fftEnergy[indexAz] * -1.3557422 + fftEnergy[indexGx] * -0.0000283 + fftEnergy[indexGy] * 0.0000506 + fftEntropy[indexAx] * 0.0051371 + fftEntropy[indexAy] * -0.0082179 + fftEntropy[indexAz] * 0.0102792 + fftEntropy[indexGx] * -0.000024 + fftEntropy[indexGz] * -0.0000173 + fftEntropy[indexTotalAcc] * -0.0075858 + fftEntropy[indexTotalGyro] * 0.000003  + covarianceAxAy * 24.9151436 + covarianceAyAz * 9.0831814 + covarianceGxGy * -0.000285 + covarianceGxGz * -0.0000108 + covarianceGyGz * 0.0000805 + correlationAxAy * -1.2083618 + correlationAyAz * -0.4802264 + correlationGxGy * 2.8685079 + correlationGxGz * 0.1352591 + correlationGyGz * 0.1532495 ;
        Class[10] = 8.1519919 +  standardDeviation[indexAx] * -5.1415759 + standardDeviation[indexGx] * -0.0226466 + standardDeviation[indexGz] * -0.0498784 + standardDeviation[indexTotalGyro] * -0.0010216 + standardDeviation[indexRoll] * -0.0173331 + standardDeviation[indexPitch] * -0.0036703 + mean[indexAz] * -6.0737974 + mean[indexGx] * -0.0011414 + mean[indexGz] * -0.0207961 + mean[indexTotalAcc] * 2.7127897 + mean[indexTotalGyro] * -0.0044479 + mean[indexRoll] * 0.0017705 + mean[indexPitch] * -0.0134215 + variance[indexAy] * 0.7379873 + variance[indexAz] * 2.6384148 + variance[indexGx] * 0.0002049 + variance[indexGy] * 0.0003083 + variance[indexGz] * 0.0000377 + variance[indexTotalGyro] * 0.0000625 + variance[indexRoll] * -0.0001699 + zeroCrossingRate[indexAy] * 3.3082196 + zeroCrossingRate[indexAz] * -1.7106782 + zeroCrossingRate[indexGx] * 2.5665363 + zeroCrossingRate[indexGy] * -3.8106038 + zeroCrossingRate[indexRoll] * -0.4756542 + rootMeanSquare[indexRoll] * 0.0238298 + skewness[indexAx] * -0.0758085 + skewness[indexAy] * -0.4044021 + skewness[indexAz] * -0.0950787 + skewness[indexGy] * -0.1048313 + skewness[indexGz] * 0.0650432 + skewness[indexTotalAcc] * -0.1984818 + skewness[indexTotalGyro] * 0.8284242 + skewness[indexPitch] * -0.0342821 + kurtosis[indexAy] * 0.115594  + kurtosis[indexAz] * 0.1443372 + kurtosis[indexTotalAcc] * 0.0699178 + kurtosis[indexTotalGyro] * 0.5091459 + kurtosis[indexRoll] * 0.0083435 + kurtosis[indexPitch] * 0.0040266 + meanAbsDev[indexAx] * 1.4577202 + meanAbsDev[indexAy] * 0.1277447 + meanAbsDev[indexGx] * -0.0678584 + meanAbsDev[indexGy] * 0.0043772 + meanAbsDev[indexGz] * 0.0355584 + meanAbsDev[indexTotalAcc] * -6.7203977 + meanAbsDev[indexTotalGyro] * -0.0162215 + meanAbsDev[indexRoll] * 0.0196535 + fftMaxFreq[indexAx] * 0.1289217 + fftMaxFreq[indexAy] * -0.0679731 + fftMaxFreq[indexGx] * 0.2707484 + fftMaxFreq[indexGz] * 0.1939555 + fftMaxFreq[indexPitch] * -0.0820876 + fftEnergy[indexAx] * 0.133012  + fftEnergy[indexAy] * -0.3792807 + fftEnergy[indexGx] * 0.0000567 + fftEnergy[indexGy] * -0.0000367 + fftEnergy[indexGz] * 0.0000278 + fftEnergy[indexTotalGyro] * 0.0000188 + fftEnergy[indexPitch] * 0.0001227 + fftEntropy[indexAx] * -0.0095836 + fftEntropy[indexAy] * 0.0159297 + fftEntropy[indexAz] * 0.0626955 + fftEntropy[indexGx] * 0.0000022 + fftEntropy[indexGz] * 0.000012  + fftEntropy[indexTotalAcc] * -0.0217899 + fftEntropy[indexTotalGyro] * -0.0000139 + fftEntropy[indexPitch] * 0.0000398 + covarianceAxAy * -0.8767856 + covarianceAyAz * 2.212326  + covarianceGxGy * -0.0000243 + covarianceGyGz * 0.0000268 + correlationAxAy * 0.0887158 + correlationGxGy * 0.6563439 + correlationGxGz * -0.0714667;
        */
        gestureDecision = getGestureLogistic(Class);

        return gestureDecision;
    }

    private double[] getTotalAxes(double[] xAxes, double[] yAxes, double[] zAxes){
        /*if(xAxes.length.equal(yAxes.length) == zAxes.length ){
            return null;
        }*/
        double[] totalAxes = new double[xAxes.length];

        for(int i=0; i< xAxes.length; i++){
            totalAxes[i] = Math.sqrt(xAxes[i] * xAxes[i] + yAxes[i] * yAxes[i] + zAxes[i] * zAxes[i]);
        }
        return totalAxes;
    }

    private double[] getRoll(double[] xAxes, double[] zAxes){
        double[] roll = new double[xAxes.length];

        for(int i=0; i< xAxes.length; i++){
            roll[i] = Math.atan2(-xAxes[i], zAxes[i]) * 180 / Math.PI;
        }

        return roll;
    }

    private double[] getPitch(double[] yAxes, double[] zAxes){
        double[] pitch = new double[yAxes.length];

        for(int i=0; i< yAxes.length; i++){
            pitch[i] = -(Math.atan2(-yAxes[i], zAxes[i]) * 180) / Math.PI;
        }

        return pitch;
    }

    private int countGestureLabel(String[] input){
        int n = input.length;
        int gestureLabelNumber =0;
        String[][] splitInput = new String[n][];

        for(int j=0; j < n; j++){
            splitInput[j] = input[j].split(",");
        }

        for(int i=0; i <n; i++){
            if(splitInput[i][0].equals("Gesture")){
                gestureLabelNumber++;
            }
        }
        return gestureLabelNumber;
    }

    private String getDominantGestureLabel(String[] input){
        String dominantGestureLabel = "";
        int n = input.length;
        String[][] splitInput = new String[n][];

        int[] gestureRecapt = new int[11];

        for(int j=0; j < n; j++){
            splitInput[j] = input[j].split(",");
        }

        for(int i =0; i < n; i++){
            switch(splitInput[i][1]){
                case "Up":
                    gestureRecapt[0]++;
                    break;
                case "Down":
                    gestureRecapt[1]++;
                    break;
                case "Left":
                    gestureRecapt[2]++;
                    break;
                case "Right":
                    gestureRecapt[3]++;
                    break;
                case "Clockwise":
                    gestureRecapt[4]++;
                    break;
                case "CounterClockwise":
                    gestureRecapt[5]++;
                    break;
                case "CrossClockwise":
                    gestureRecapt[6]++;
                    break;
                case "CrossCounterClockwise":
                    gestureRecapt[7]++;
                    break;
                case "VLR":
                    gestureRecapt[8]++;
                    break;
                case "VRL":
                    gestureRecapt[9]++;
                    break;
                case "NonGesture":
                    gestureRecapt[10]++;
                    break;
            }
        }
        int max =0;

        for (int counter = 0; counter < gestureRecapt.length; counter++)
        {
            if (gestureRecapt[counter] > max)
            {
                max = gestureRecapt[counter];
            }
        }

        if(gestureRecapt[0]==max){
            dominantGestureLabel = "Up";
        } else if(gestureRecapt[1]==max){
            dominantGestureLabel = "Down";
        } else if(gestureRecapt[2]==max){
            dominantGestureLabel = "Left";
        } else if(gestureRecapt[3]==max){
            dominantGestureLabel = "Right";
        } else if(gestureRecapt[4]==max){
            dominantGestureLabel = "Clockwise";
        } else if(gestureRecapt[5]==max){
            dominantGestureLabel = "CounterClockwise";
        } else if(gestureRecapt[6]==max){
            dominantGestureLabel = "CrossClockwise";
        } else if(gestureRecapt[7]==max){
            dominantGestureLabel = "CrossCounterClockwise";
        } else if(gestureRecapt[8]==max){
            dominantGestureLabel = "VLR";
        } else if(gestureRecapt[9]==max){
            dominantGestureLabel = "VRL";
        } else if(gestureRecapt[10]==max){
            dominantGestureLabel = "NonGesture";
        }
        //Log.d("AHOY","Biggest number: " + max);

        // Get the biggest values' variable
        //if(up > down)
        //Log.d("AHOY",Integer.toString(Math.max(up,down)));

        return dominantGestureLabel;
    }

    private String getGestureLogistic(double[] allClassProbability){
        String gesture = "";
        double max =0;
        double totalClass = 0;
        double[] classCore = new double[allClassProbability.length];

        for(int i=0; i< allClassProbability.length; i++){
            totalClass = totalClass + Math.exp(allClassProbability[i]);
        }
        for(int j=0; j<allClassProbability.length; j++){
            classCore[j]=Math.exp(allClassProbability[j])/totalClass;
        }
        //Math.exp(class0) + Math.exp(class1) + Math.exp(class2)+ Math.exp(class3) + Math.exp(class4) + Math.exp(class5) + Math.exp(class6) + Math.exp(class7) + Math.exp(class8) + Math.exp(class9) + Math.exp(class10);
        for (int counter = 0; counter < classCore.length; counter++)
        {
            if (classCore[counter] > max)
            {
                max = classCore[counter];
            }
        }

        if(classCore[0]==max){
            gesture = "Up";
        } else if(classCore[1]==max){
            gesture = "Down";
        } else if(classCore[2]==max){
            gesture = "Left";
        } else if(classCore[3]==max){
            gesture = "Right";
        } else if(classCore[4]==max){
            gesture = "VLR";
        } else if(classCore[5]==max){
            gesture = "VRL";
        } else if(classCore[6]==max){
            gesture = "CrossClockwise";
        } else if(classCore[7]==max){
            gesture = "CrossCounterClockwise";
        } else if(classCore[8]==max){
            gesture = "Clockwise";
        } else if(classCore[9]==max){
            gesture = "CounterClockwise";
        } else if(classCore[10]==max){
            gesture = "NonGesture";
        }

        return gesture;
    }

    // MATH OPERATIONS ARE HERE
    private double getMean(double[] input) {
        double mean;
        double sum = 0;
        int n = input.length;

        for (int i = 0; i < n; i++) {
            sum = sum + input[i];
        }
        mean = sum / n;
        return mean;
    }

    private double[] getMean2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];

        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getMean(temp2);
        }
        return output;
    }

    private double getVariance(double[] input) {
        double variance = 0;
        double mean = getMean(input);
        double delta = 0;

        int n = input.length;

        for (int i = 0; i < n; i++) {
            delta = input[i] - mean;
            variance = variance + (delta * delta);
        }

        variance = variance / (n - 1);
        return variance;
    }

    private double[] getVariance2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getVariance(temp2);
        }
        return output;
    }

    private double getVariance2(double[] input) {
        double variance = 0;
        double mean = getMean(input);
        double delta = 0;

        int n = input.length;

        for (int i = 0; i < n; i++) {
            delta = input[i] - mean;
            variance = variance + (delta * delta);
        }

        variance = variance / n;
        return variance;
    }

    private double getCovariance(double[] input1, double[] input2) {
        double cov = 0;
        double mean1 = getMean(input1);
        double mean2 = getMean(input2);

        if(input1.length!=input2.length){
            throw new IllegalArgumentException("Two input matrices should have same dimension");
        }

        for (int i = 0; i < input1.length; i++) {
            cov += (input1[i] - mean1) * (input2[i] - mean2);
        }

        return cov / (input1.length - 1);
    }

    private double getCorrelation(double[] input1, double[] input2){
        double cov = getCovariance(input1,input2);
        double standardDeviation1 = getStandardDeviation(input1);
        double standardDeviation2 = getStandardDeviation(input2);

        if(input1.length!=input2.length){
            throw new IllegalArgumentException("Two input matrices should have same dimension");
        }

        double correl = cov / (standardDeviation1 * standardDeviation2);

        return correl;
    }

    private double getZeroCrossingRate(double[] input) {
        double zcr = 0;

        for (int i = 0; i < (input.length - 1); i++) {
            if ((input[i] * input[i + 1]) < 0)
                zcr++;
        }
        zcr = zcr / input.length;
        return zcr;
    }

    private double[] getZeroCrossingRate2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getZeroCrossingRate(temp2);
        }
        return output;
    }

    private double getStandardDeviation(double[] input) {
        double standardDeviation = 0;
        double variance = getVariance(input);

        standardDeviation = Math.sqrt(variance);
        return standardDeviation;
    }

    private double[] getStandardDeviation2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getStandardDeviation(temp2);
        }
        return output;
    }

    private double getStandardDeviation2(double[] input) {
        double standardDeviation = 0;
        double variance = getVariance2(input);

        standardDeviation = Math.sqrt(variance);
        return standardDeviation;
    }

    private double getRootMeanSquare(double[] input) {
        double rms = 0;

        for (int i = 0; i < input.length; i++) {
            rms += Math.pow(input[i], 2);
        }

        return (double)Math.pow(rms/input.length, 0.5);
    }

    private double[] getRootMeanSquare2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getRootMeanSquare(temp2);
        }
        return output;
    }

    private double getSkewness(double[] input) {
        double skew = 0;
        double mean = getMean(input);
        double std = getStandardDeviation2(input);

        for (int i = 0; i < input.length; i++) {
            skew = skew + Math.pow(input[i] - mean, 3);
        }

        skew = skew / (input.length * Math.pow(std,3));
        return skew;
    }

    private double[] getSkewness2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getSkewness(temp2);
        }
        return output;
    }

    private double getKurtosis(double[] input) {
        double kurt = 0;
        double mean = getMean(input);
        double std = getStandardDeviation2(input);

        for (int i = 0; i < input.length; i++) {
            kurt = kurt + Math.pow(input[i] - mean, 4);
        }

        kurt = kurt / (input.length * Math.pow(std,4));

        return kurt;
    }

    private double[] getKurtosis2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getKurtosis(temp2);
        }
        return output;
    }

    private double getMeanAbsDev(double[] input) {
        double mad = 0;
        double mean = getMean(input);

        for (int i = 0; i < input.length; i++) {
            mad += Math.abs(input[i] - mean);
        }

        return mad/input.length;
    }

    private double[] getMeanAbsDev2D(double[][] input){
        int col = 0;
        int row = 0;

        col = input.length;
        row = input[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(input,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getMeanAbsDev(temp2);
        }
        return output;
    }

    // function if you don't have imaginary vector
    private double[] fft_magnitude(double[] inputReal, boolean DIRECT) {
        // - n is the dimension of the problem
        // - nu is its logarithm in base e
        int n = inputReal.length;
        int nClosest = getClosestPowerofTwo(n);

        // Since, we don't have imagener, just generate the empty vector
        double[] inputImag = new double[nClosest];

        // If n is a power of 2, then ld is an integer (_without_ decimals)
        double ld = Math.log(nClosest) / Math.log(2.0);

        // Here I check if n is a power of 2. If exist decimals in ld, I quit
        // from the function returning null.
        if (((int) ld) - ld != 0) {
            System.out.println("The number of elements is not a power of 2.");
            return null;
        }

        // Declaration and initialization of the variables
        // ld should be an integer, actually, so I don't lose any information in
        // the cast
        int nu = (int) ld;
        int n2 = nClosest / 2;
        int nu1 = nu - 1;
        double[] xReal = new double[nClosest];
        double[] xImag = new double[nClosest];
        double tReal, tImag, p, arg, c, s;

        // Here I check if I'm going to do the direct transform or the inverse
        // transform.
        double constant;
        if (DIRECT)
            constant = -2 * Math.PI;
        else
            constant = 2 * Math.PI;

        // I don't want to overwrite the input arrays, so here I copy them. This
        // choice adds \Theta(2n) to the complexity.
        for (int i = 0; i < nClosest; i++) {
            xReal[i] = inputReal[i];
            xImag[i] = inputImag[i];
        }

        // First phase - calculation
        int k = 0;
        for (int l = 1; l <= nu; l++) {
            while (k < nClosest) {
                for (int i = 1; i <= n2; i++) {
                    p = bitreverseReference(k >> nu1, nu);
                    // direct FFT or inverse FFT
                    arg = constant * p / nClosest;
                    c = Math.cos(arg);
                    s = Math.sin(arg);
                    tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                    tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                    xReal[k + n2] = xReal[k] - tReal;
                    xImag[k + n2] = xImag[k] - tImag;
                    xReal[k] += tReal;
                    xImag[k] += tImag;
                    k++;
                }
                k += n2;
            }
            k = 0;
            nu1--;
            n2 /= 2;
        }

        // Second phase - recombination
        k = 0;
        int r;
        while (k < nClosest) {
            r = bitreverseReference(k, nu);
            if (r > k) {
                tReal = xReal[k];
                tImag = xImag[k];
                xReal[k] = xReal[r];
                xImag[k] = xImag[r];
                xReal[r] = tReal;
                xImag[r] = tImag;
            }
            k++;
        }

        // Here I have to mix xReal and xImag to have an array (yes, it should
        // be possible to do this stuff in the earlier parts of the code, but
        // it's here to readibility).
        double[] newArray = new double[xReal.length];
        double radice = 1 / Math.sqrt(nClosest);
        for (int i = 0; i < newArray.length; i++) {

            // I Modified Original Program so that it will only produce fft magnitude array.
            newArray[i] = (double)Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)),0.5);
        }
        return newArray;
    }

    // function if you have imaginary vector
    private double[] fft_magnitude(double[] inputReal, double[] inputImag, boolean DIRECT) {
        // - n is the dimension of the problem
        // - nu is its logarithm in base e
        int n = inputReal.length;
        int nClosest = 0;

        // get the closest number
        if(0< n && n <=2){
            nClosest = 0;
        } else if(2< n && n<=4) {
            nClosest = 2;
        } else if(4< n && n<=8) {
            nClosest = 4;
        } else if(8<n && n<=16) {
            nClosest = 8;
        } else if(16< n && n <=64) {
            nClosest = 16;
        } else if(64< n && n <=128) {
            nClosest = 64;
        } else if(128< n && n <=256){
            nClosest = 128;
        } else if (256< n && n <=512){
            nClosest = 256;
        }

        // If n is a power of 2, then ld is an integer (_without_ decimals)
        double ld = Math.log(nClosest) / Math.log(2.0);

        // Here I check if n is a power of 2. If exist decimals in ld, I quit
        // from the function returning null.
        if (((int) ld) - ld != 0) {
            System.out.println("The number of elements is not a power of 2.");
            return null;
        }

        // Declaration and initialization of the variables
        // ld should be an integer, actually, so I don't lose any information in
        // the cast
        int nu = (int) ld;
        int n2 = nClosest / 2;
        int nu1 = nu - 1;
        double[] xReal = new double[nClosest];
        double[] xImag = new double[nClosest];
        double tReal, tImag, p, arg, c, s;

        // Here I check if I'm going to do the direct transform or the inverse
        // transform.
        double constant;
        if (DIRECT)
            constant = -2 * Math.PI;
        else
            constant = 2 * Math.PI;

        // I don't want to overwrite the input arrays, so here I copy them. This
        // choice adds \Theta(2n) to the complexity.
        for (int i = 0; i < nClosest; i++) {
            xReal[i] = inputReal[i];
            xImag[i] = inputImag[i];
        }

        // First phase - calculation
        int k = 0;
        for (int l = 1; l <= nu; l++) {
            while (k < nClosest) {
                for (int i = 1; i <= n2; i++) {
                    p = bitreverseReference(k >> nu1, nu);
                    // direct FFT or inverse FFT
                    arg = constant * p / nClosest;
                    c = Math.cos(arg);
                    s = Math.sin(arg);
                    tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                    tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                    xReal[k + n2] = xReal[k] - tReal;
                    xImag[k + n2] = xImag[k] - tImag;
                    xReal[k] += tReal;
                    xImag[k] += tImag;
                    k++;
                }
                k += n2;
            }
            k = 0;
            nu1--;
            n2 /= 2;
        }

        // Second phase - recombination
        k = 0;
        int r;
        while (k < nClosest) {
            r = bitreverseReference(k, nu);
            if (r > k) {
                tReal = xReal[k];
                tImag = xImag[k];
                xReal[k] = xReal[r];
                xImag[k] = xImag[r];
                xReal[r] = tReal;
                xImag[r] = tImag;
            }
            k++;
        }

        // Here I have to mix xReal and xImag to have an array (yes, it should
        // be possible to do this stuff in the earlier parts of the code, but
        // it's here to readibility).
        double[] newArray = new double[xReal.length];
        double radice = 1 / Math.sqrt(nClosest);
        for (int i = 0; i < newArray.length; i++) {

            // I Modified Original Program so that it will only produce fft magnitude array.
            newArray[i] = (double)Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)),0.5);
        }
        return newArray;
    }

    private int bitreverseReference(int j, int nu) {
        int j2;
        int j1 = j;
        int k = 0;
        for (int i = 1; i <= nu; i++) {
            j2 = j1 / 2;
            k = 2 * k + j1 - 2 * j2;
            j1 = j2;
        }
        return k;
    }

    private int getClosestPowerofTwo(int input){

        // loga (b) = c
        // a^c = b
        // b = input

        double cDouble = Math.log10(input)/Math.log10(2);
        int cInt = (int) cDouble;

        int nClosest = (int) Math.pow(2,cInt);

        return nClosest;
    }

    private double getFFTMaxFreq(double[] inputReal, double freqSampling) {
        double[] fftVector = fft_magnitude(inputReal,true);

        double max = 0;
        double max_freq = 0;
        int n = inputReal.length;

        // get the closest number
        int nClosest = getClosestPowerofTwo(n);

        double n_data = 2 * nClosest;
        double fft_resolution = freqSampling/n_data;

        //I only check until half spectrum ~5Hz
        for (int i = 0; i < fftVector.length/2; i++) {
            if (fftVector[i] > max) {
                max = fftVector[i];
                max_freq = fft_resolution * i;
            }
        }
        return max_freq;
    }

    private double getFFTMaxFreq(double[] inputReal, double[] inputImag, double freqSampling) {
        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        double max = 0;
        double max_freq = 0;
        int n = inputReal.length;

        // get the closest number
        int nClosest = getClosestPowerofTwo(n);

        double n_data = 2 * nClosest;
        double fft_resolution = freqSampling/n_data;

        //I only check until half spectrum ~5Hz
        for (int i = 0; i < fftVector.length/2; i++) {
            if (fftVector[i] > max) {
                max = fftVector[i];
                max_freq = fft_resolution * i;
            }
        }
        return max_freq;
    }

    private double[] getFFTMaxFreq2D(double[][] inputReal, double freqSampling){
        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTMaxFreq(temp2,freqSampling);
        }
        return output;
    }

    private double getFFTEnergy(double[] inputReal) {
        double energy = 0;
        double[] fftVector = fft_magnitude(inputReal,true);

        for (int i = 0; i < fftVector.length; i++) {
            energy += Math.pow(fftVector[i], 2);
        }

        return energy/fftVector.length;
    }

    private double getFFTEnergy(double[] inputReal, double[] inputImag) {
        double energy = 0;
        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        for (int i = 0; i < fftVector.length; i++) {
            energy += Math.pow(fftVector[i], 2);
        }

        return energy/fftVector.length;
    }

    private double[] getFFTEnergy2D(double[][] inputReal){
        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTEnergy(temp2);
        }
        return output;
    }

    private double getFFTEntropy(double[] inputReal) {
        double entropy = 0;
        double[] fftVector = fft_magnitude(inputReal,true);

        for (int i = 0; i < fftVector.length; i++) {
            entropy += Math.log(fftVector[i])*fftVector[i];
        }

        return -entropy;
    }

    private double getFFTEntropy(double[] inputReal, double[] inputImag) {
        double entropy = 0;
        double[] fftVector = fft_magnitude(inputReal,inputImag,true);

        for (int i = 0; i < fftVector.length; i++) {
            entropy += Math.log(fftVector[i])*fftVector[i];
        }

        return -entropy;
    }

    private double[] getFFTEntropy2D(double[][] inputReal){
        int col = 0;
        int row = 0;

        col = inputReal.length;
        row = inputReal[0].length;

        double[] output = new double[col];
        double[][] temp = reshape(inputReal,col*row,1);

        double[] temp2 = new double[row];
        for(int f=0; f<col; f++){
            for(int g=0; g<row; g++){
                temp2[g]=temp[f*row+g][0];
            }
            output[f] = getFFTEntropy(temp2);
        }
        return output;
    }

    private int getPermutations(int input){
        int permutation = 1;
        if(input != 0){
            for (int i = 1; i<= input; i++) {
                permutation = permutation * i;
            }
        }
        else{
            throw new IllegalArgumentException("N and K, can not be zero!");
        }
        return permutation;
    }

    private int getCombinations(int n, int k){
        int num = 1;
        int denum = 1;
        int combination;
        if( n !=0 && k != 0 ){
            num = getPermutations(n);
            denum = getPermutations(k) * getPermutations(n-k);

            combination = num / denum;
        }
        else{
            throw new IllegalArgumentException("N and K, can not be zero!");
        }
        return combination;
    }

    public static double[][] reshape(double[][] A, int m, int n) {
        int origM = A.length;
        int origN = A[0].length;
        if(origM*origN != m*n){
            throw new IllegalArgumentException("New matrix must be of same area as matix A");
        }
        double[][] B = new double[m][n];
        double[] A1D = new double[A.length * A[0].length];

        int index = 0;
        for(int i = 0;i<A.length;i++){
            for(int j = 0;j<A[0].length;j++){
                A1D[index++] = A[i][j];
            }
        }

        index = 0;
        for(int i = 0;i<n;i++){
            for(int j = 0;j<m;j++){
                B[j][i] = A1D[index++];
            }

        }
        return B;
    }
}
