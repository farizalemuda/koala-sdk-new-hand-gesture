package cc.nctu1210.sample.koala6x;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobotFinder;
import com.wowwee.bluetoothrobotcontrollib.sdk.MipRobot.MipRobotInterface;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import cc.nctu1210.api.koala6x.KoalaDevice;
import cc.nctu1210.api.koala6x.KoalaGesture;
import cc.nctu1210.api.koala6x.KoalaService;
import cc.nctu1210.api.koala6x.KoalaServiceManager;
import cc.nctu1210.api.koala6x.SensorEvent;
import cc.nctu1210.api.koala6x.SensorEventListener;
import cc.nctu1210.view.CustomAdapter;
import cc.nctu1210.view.ModelObject;

public class MainActivity extends FragmentActivity implements AdapterView.OnItemClickListener, SensorEventListener, MipRobotInterface {
    private final static String TAG = MainActivity.class.getSimpleName();

    private boolean startScan = false;
    private KoalaServiceManager mServiceManager;
    private BluetoothAdapter mBluetoothAdapter;
    /******** for SDK version > 21 **********/
    private BluetoothLeScanner mBLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    /******** for SDK version > 21 **********/
    public static ArrayList<KoalaDevice> mDevices = new ArrayList<KoalaDevice>();  // Manage the devices
    public static ArrayList<AtomicBoolean> mFlags = new ArrayList<AtomicBoolean>();

    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 2000;
    public static final int REQUEST_CODE = 30;

    private Button btScan;
    private Button btStart;

    /* ListView Related */
    private String DEVICE_NAME = "name";
    private String DEVICE_ADDRESS = "address";
    private String DEVICE_RSSI = "rssi";
    private String DEVICE_GX = "gX";
    private String DEVICE_GY = "gY";
    private String DEVICE_GZ = "gZ";
    private ListView listView;
    private List<ModelObject> mObjects = new ArrayList<ModelObject>();
    private CustomAdapter mAdapter;

    private void displayRSSI(final int position, final float rssi) {
        ModelObject object = mObjects.get(position);
        object.setRssi(rssi);
        mAdapter.notifyDataSetChanged();
    }

    private void displayAccData(final int position, final double acc[]) {
        ModelObject object = mObjects.get(position);
        object.setAccelerometerData(acc[0], acc[1], acc[2]);
        mAdapter.notifyDataSetChanged();
    }

    private void displayPDRData(final int position, final float pdrData[]) {
        ModelObject object = mObjects.get(position);
        object.setPedometerData(pdrData[0], pdrData[1], pdrData[2], pdrData[3]);
        mAdapter.notifyDataSetChanged();
    }

    private void updateSamplingRate(final int position, float sampling) {
        ModelObject object = mObjects.get(position);
        object.setSampling(sampling);
        mAdapter.notifyDataSetChanged();
    }

    KoalaGesture koalaGesture = new KoalaGesture();
    int dataSampling = 180;
    int koalaRawData = 6;
    double[][] koalaData = new double[koalaRawData][dataSampling];
    String gesture = "";
    TextView txtGesture;
    int counter = 0;

    protected float[] forwardMovement = new float[]{ 0, (float)32.0};
    protected float[] backwardMovement = new float[]{ 0, (float)-10.0};
    protected float[] CWMovement = new float[]{ 1, (float)0};
    protected float[] CCWMovement = new float[]{ -1, (float)0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        btScan = (Button) findViewById(R.id.bt_scan);
        btStart = (Button) findViewById(R.id.bt_log);
        listView = (ListView) findViewById(R.id.listView);
        txtGesture = (TextView) findViewById(R.id.txtGesture);

        mAdapter = new CustomAdapter(this, mObjects);

        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);

        btScan.setOnClickListener(scanListener);

        Log.i(TAG, "getPackageManager");
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
                    .show();
            finish();
        }

        final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
                    .show();
            finish();
            return;
        }


        mServiceManager = new KoalaServiceManager(MainActivity.this);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_ACCELEROMETER, KoalaService.MOTION_WRITE_RATE_10, KoalaService.MOTION_ACCEL_SCALE_2G, KoalaService.MOTION_GYRO_SCALE_250);
        //mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_ACCELEROMETER);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_GYROSCOPE);

        // MIP
        // Set BluetoothAdapter to MipRobotFinder
        MipRobotFinder.getInstance().setBluetoothAdapter(mBluetoothAdapter);

        // Set Context to MipRobotFinder
        MipRobotFinder finder = MipRobotFinder.getInstance();
        finder.setApplicationContext(getApplicationContext());
    }

    private Button.OnClickListener scanListener = new Button.OnClickListener() {
        public void onClick(View v) {
            if (!startScan) {
                startScan = true;
                btScan.setText("Stop scan");
                mDevices.clear();
                mFlags.clear();
                // Start to scan the ble device
                scanLeDevice();

                try {
                    Thread.sleep(SCAN_PERIOD+1000);

                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                setupListView();
            } else {
                startScan = false;
                mServiceManager.disconnect();
                mServiceManager.close();
                mDevices.clear();
                mFlags.clear();
                setupListView();
                btScan.setText("Scan");
            }

        }
    };

    private void setupListView() {
        mAdapter.getData().clear();
        Log.i(TAG, "Initializing ListView....."+ mAdapter.getData().size());
        for (int i = 0, size = mDevices.size(); i < size; i++) {
            KoalaDevice d = mDevices.get(i);
            ModelObject object = new ModelObject(d.getDevice().getName(), d.getDevice().getAddress(), String.valueOf(d.getRssi()));
            mObjects.add(object);
        }
        Log.i(TAG, "Initialized ListView....."+ mAdapter.getData().size());

        mAdapter.notifyDataSetChanged();

    }

    private Button.OnClickListener logListener = new Button.OnClickListener() {
        public void onClick(View v) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        this.registerReceiver(mMipFinderBroadcastReceiver, MipRobotFinder.getMipRobotFinderIntentFilter());
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                TextView noBtText = (TextView)this.findViewById(R.id.no_bt_text);
                noBtText.setVisibility(View.VISIBLE);
            }
        }

        // Search for mip
        MipRobotFinder.getInstance().clearFoundMipList();
        scanMIPRobot(false);
        //updateMipList();
        scanMIPRobot(true);

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                mBLEScanner =mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
            }
        }



    }

    private void scanLeDevice() {
        new Thread() {

            @Override
            public void run() {

                if (Build.VERSION.SDK_INT < 21) {
                    mBluetoothAdapter.startLeScan(mLeScanCallback);

                    try {
                        Thread.sleep(SCAN_PERIOD);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                } else {
                    mBLEScanner.startScan(mScanCallback);

                    try {
                        Thread.sleep(SCAN_PERIOD);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    mBLEScanner.stopScan(mScanCallback);
                }

                /*
                runOnUiThread(new Runnable() {
		            @Override
		            public void run() {
		                // This code will always run on the UI thread, therefore is safe to modify UI elements.
		            	setupListView();
		            }
		        });
		        */
            }
        }.start();
    }

    /**
     * The event callback to handle the found of near le devices
     * For SDK version < 21.
     *
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {

            new Thread() {
                @Override
                public void run() {
                    if (device != null) {
                        KoalaDevice p = new KoalaDevice(device, rssi, scanRecord);
                        int position = findKoalaDevice(device.getAddress());
                        if (position == -1) {
                            AtomicBoolean flag = new AtomicBoolean(false);
                            mDevices.add(p);
                            mFlags.add(flag);
                            Log.i(TAG, "Find device:"+p.getDevice().getAddress());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                    setupListView();
                                }
                            });
                        }
                    }
                }
            }.start();
        }
    };

    /**
     * The event callback to handle the found of near le devices
     * For SDK version >= 21.
     *
     */

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.i("callbackType", String.valueOf(callbackType));
            Log.i("result", result.toString());
            final ScanResult scanResult = result;
            final BluetoothDevice device = scanResult.getDevice();

            new Thread() {
                @Override
                public void run() {
                    if (device != null) {
                        KoalaDevice p = new KoalaDevice(device, scanResult.getRssi(), scanResult.getScanRecord().getBytes());
                        int position = findKoalaDevice(device.getAddress());
                        if (position == -1) {
                            AtomicBoolean flag = new AtomicBoolean(false);
                            mDevices.add(p);
                            mFlags.add(flag);
                            Log.i(TAG, "Find device:"+p.getDevice().getAddress());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                    setupListView();
                                }
                            });
                        }
                    }
                }
            }.start();
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };

    private int findKoalaDevice(String macAddr) {
        if (mDevices.size() == 0)
            return -1;
        for (int i=0; i<mDevices.size(); i++) {
            KoalaDevice tmpDevice = mDevices.get(i);
            if (macAddr.matches(tmpDevice.getDevice().getAddress()))
                return i;
        }
        return -1;
    }

    @Override
    protected void onStop() {
        super.onStop();
        for (int i=0; i<mFlags.size(); i++) {
            AtomicBoolean flag = mFlags.get(i);
            flag.set(false);
        }

        this.unregisterReceiver(mMipFinderBroadcastReceiver);
        for(MipRobot mip : MipRobotFinder.getInstance().getMipsConnected()) {
            mip.readMipHardwareVersion();
            mip.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        System.exit(0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        String macAddress = mAdapter.getData().get(position).getAddress();
        KoalaDevice d = mDevices.get(position);
        Log.d(TAG, "Connecting to device:"+macAddress);
        d.resetSamplingRate();
        d.setConnectedTime();
        mServiceManager.connect(macAddress);
    }

    @Override
    public void onSensorChange(final SensorEvent e) {
        final int eventType = e.type;
        final double values [] = new double[3];
        switch (eventType) {
            case SensorEvent.TYPE_ACCELEROMETER:
                final int acc_position = findKoalaDevice(e.device.getAddress());
                if (acc_position != -1) {
                    final KoalaDevice d = mDevices.get(acc_position);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                d.addRecvItem();
                                values[0] = e.values[0];
                                values[1] = e.values[1];
                                values[2] = e.values[2];

                                for (int a = 0; a < 3; a++) {
                                    for (int b = 0; b < (dataSampling - 1); b++) {
                                        koalaData[a][b] = koalaData[a][b + 1];
                                    }
                                }

                                koalaData[0][dataSampling-1]= e.values[0];
                                koalaData[1][dataSampling-1]= e.values[1];
                                koalaData[2][dataSampling-1]= e.values[2];
                                counter++;
                                Log.d(TAG, "time="+System.currentTimeMillis()+ "gX:" + values[0]+"gY:" + values[1]+"gZ:" + values[2]+"\n");
                                updateSamplingRate(acc_position, d.getCurrentSamplingRate());
                                displayAccData(acc_position, values);
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                        }
                    });
                }
                break;
            case SensorEvent.TYPE_GYROSCOPE:
                final int gyro_position = findKoalaDevice(e.device.getAddress());
                if (gyro_position != -1) {
                    final KoalaDevice d = mDevices.get(gyro_position);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                //d.addRecvItem();
                                values[0] = e.values[0];
                                values[1] = e.values[1];
                                values[2] = e.values[2];

                                for (int a = 3; a < 6; a++) {
                                    for (int b = 0; b < (dataSampling - 1); b++) {
                                        koalaData[a][b] = koalaData[a][b + 1];
                                    }
                                }

                                koalaData[3][dataSampling-1]= e.values[0];
                                koalaData[4][dataSampling-1]= e.values[1];
                                koalaData[5][dataSampling-1]= e.values[2];
                                Log.d(TAG, "time="+System.currentTimeMillis()+ "aX:"
                                        + values[0]+"aY:" + values[1]+"aZ:" + values[2]+"\n");
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                        }
                    });
                }
                break;
        }
        if(counter>10){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    koalaGesture.setKoalaData(koalaData);
                    gesture=koalaGesture.getGesture();
                    switch(gesture){
                        case "Up":
                            for(int a=0; a<10; a++){
                                mipDrive(forwardMovement);
                            }
                            break;
                        case "Down":
                            for(int a=0; a<10; a++){
                                mipDrive(backwardMovement);
                            }
                            break;
                        case "Clockwise":
                            for(int a=0; a<50; a++){
                                mipDrive(CWMovement);
                            }
                            break;
                        case "CounterClockwise":
                            for(int b=0; b<50; b++){
                                mipDrive(CCWMovement);
                            }
                            break;
                    }
                    txtGesture.setText(gesture);
                    Log.d("KOALACWT",gesture);
                    counter=0;
                }
            });
        }

    }

    @Override
    public void onConnectionStatusChange(boolean status) {

    }

    @Override
    public void onRSSIChange(String addr, float rssi) {
        final int position = findKoalaDevice(addr);
        if (position != -1) {
            Log.d(TAG, "mac Address:"+addr+" rssi:"+rssi);
            displayRSSI(position, rssi);
        }
    }

    @Override
    public void mipDeviceReady(MipRobot sender) {
        final MipRobot robot = sender;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Connected: "+robot.getName());
            }
        });
    }

    @Override
    public void mipDeviceDisconnected(MipRobot mipRobot) {
        final MipRobot robot = mipRobot;
        Log.d("AHOY","DISCONNECTED");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Disconnected: "+robot.getName());
            }
        });
    }

    @Override
    public void mipRobotDidReceiveBatteryLevelReading(MipRobot mipRobot, int i) {

    }

    @Override
    public void mipRobotDidReceivePosition(MipRobot mipRobot, byte b) {

    }

    @Override
    public void mipRobotDidReceiveHardwareVersion(int i, int i1) {

    }

    @Override
    public void mipRobotDidReceiveSoftwareVersion(Date date, int i) {

    }

    @Override
    public void mipRobotDidReceiveVolumeLevel(int i) {

    }

    @Override
    public void mipRobotDidReceiveIRCommand(ArrayList<Byte> arrayList, int i) {

    }

    @Override
    public void mipRobotDidReceiveWeightReading(byte b, boolean b1) {

    }

    @Override
    public void mipRobotIsCurrentlyInBootloader(MipRobot mipRobot, boolean b) {

    }

    private void scanMIPRobot(final boolean enable) {
        if (enable) {
            MipRobotFinder.getInstance().scanForMips();
        } else {
            MipRobotFinder.getInstance().stopScanForMips();
        }
    }

    private void connectToMip(final MipRobot mip) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mip.setCallbackInterface(MainActivity.this);
                mip.connect(MainActivity.this.getApplicationContext());
                TextView connectionView = (TextView)MainActivity.this.findViewById(R.id.connect_text);
                connectionView.setText("Connecting: "+mip.getName());
            }
        });
    }

    private final BroadcastReceiver mMipFinderBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (MipRobotFinder.MipRobotFinder_MipFound.equals(action)) {
                // Connect to mip
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        List<MipRobot> mipFoundList = MipRobotFinder.getInstance().getMipsFoundList();
                        if (mipFoundList != null && mipFoundList.size() > 0){
                            MipRobot selectedMipRobot = mipFoundList.get(0);
                            if (selectedMipRobot != null){
                                connectToMip(selectedMipRobot);
                            }
                        }
                    }
                }, 1);
            }
        }
    };

    public void mipDrive(float[] vector) {
        List<MipRobot> mips = MipRobotFinder.getInstance().getMipsConnected();

        for (MipRobot mip : mips) {
            mip.mipDrive(vector);
        }
    }
}
